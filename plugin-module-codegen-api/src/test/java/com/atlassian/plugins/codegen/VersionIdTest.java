package com.atlassian.plugins.codegen;

import org.junit.Test;

import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class VersionIdTest {

    private static final VersionId JUST_VERSION = VersionId.version("foo");
    private static final VersionId NO_VERSION = VersionId.noVersion();
    private static final VersionId VERSION_PROPERTY = VersionId.versionProperty("prop", "def");

    private static void assertVersionOrPropertyPlaceholder(final VersionId versionId, final String expectedValue) {
        // Act
        final Optional<String> versionOrPropertyPlaceholder = versionId.getVersionOrPropertyPlaceholder();

        // Assert
        assertThat(versionOrPropertyPlaceholder, is(Optional.ofNullable(expectedValue)));
    }

    @Test
    public void getVersionOrPropertyPlaceholder_whenNoVersion_shouldReturnEmpty() {
        assertVersionOrPropertyPlaceholder(NO_VERSION, null);
    }

    @Test
    public void getVersionOrPropertyPlaceholder_whenJustVersion_shouldReturnThatVersion() {
        assertVersionOrPropertyPlaceholder(JUST_VERSION, "foo");
    }

    @Test
    public void getVersionOrPropertyPlaceholder_whenDefaultedProperty_shouldReturnPropertyPlaceholder() {
        assertVersionOrPropertyPlaceholder(VERSION_PROPERTY, "${prop}");
    }

    private static void assertToString(final VersionId versionId, final String expectedValue) {
        assertThat(versionId.toString(), is(expectedValue));
    }

    @Test
    public void toString_whenNoVersion_shouldReturnQuestionMark() {
        assertToString(NO_VERSION, "?");
    }

    @Test
    public void toString_whenJustVersion_shouldReturnThatVersion() {
        assertToString(JUST_VERSION, "foo");
    }

    @Test
    public void toString_whenDefaultedProperty_shouldReturnPlaceholderPlusDefault() {
        assertToString(VERSION_PROPERTY, "${prop} (def)");
    }
}
