package com.atlassian.plugins.codegen;

import java.util.Optional;

import static java.util.Objects.requireNonNull;

/**
 * Describes a line item in a bundle instruction element, such as &lt;Import-Package&gt;,
 * that should be added to the POM.
 */
public final class BundleInstruction implements PluginProjectChange, SummarizeAsGroup {

    public enum Category {
        IMPORT("Import-Package"),
        PRIVATE("Private-Package"),
        DYNAMIC_IMPORT("DynamicImport-Package");

        private String elementName;

        private Category(String elementName) {
            this.elementName = elementName;
        }

        public String getElementName() {
            return elementName;
        }
    }

    private Category category;
    private String packageName;
    private Optional<String> version;

    public static BundleInstruction importPackage(String packageName, String version) {
        return new BundleInstruction(Category.IMPORT, packageName, Optional.of(version));
    }

    public static BundleInstruction dynamicImportPackage(String packageName, String version) {
        return new BundleInstruction(Category.DYNAMIC_IMPORT, packageName, Optional.of(version));
    }

    public static BundleInstruction privatePackage(String packageName) {
        return new BundleInstruction(Category.PRIVATE, packageName, Optional.empty());
    }

    public BundleInstruction(Category category, String packageName, Optional<String> version) {
        this.category = requireNonNull(category, "category");
        this.packageName = requireNonNull(packageName, "packageName");
        this.version = requireNonNull(version, "version");
    }

    public Category getCategory() {
        return category;
    }

    public String getPackageName() {
        return packageName;
    }

    public Optional<String> getVersion() {
        return version;
    }

    @Override
    public String getGroupName() {
        return "bundle instructions";
    }

    @Override
    public String toString() {
        return "[bundle instruction: " + category.getElementName() + " " + packageName + "]";
    }
}
