package com.atlassian.plugins.codegen.modules.common.web;

import com.atlassian.plugins.codegen.modules.common.Resource;
import com.atlassian.plugins.codegen.modules.common.ResourcedProperties;

import java.util.ArrayList;
import java.util.List;

import static java.util.Arrays.asList;
import static java.util.Collections.unmodifiableList;

/**
 * The properties of a <a href="https://developer.atlassian.com/server/framework/atlassian-sdk/web-resource-plugin-module/">
 * {@code web-resource}</a> plugin module.
 *
 * @since 3.6
 */
public class WebResourceProperties extends AbstractConditionsProperties implements ResourcedProperties {
    public static final String RESOURCES = "RESOURCES";
    public static final String DEPENDENCIES = "DEPENDENCIES";
    public static final String CONTEXTS = "CONTEXTS";
    public static final String TRANSFORMATIONS = "TRANSFORMATIONS";

    private static final List<String> KNOWN_CONTEXTS = asList(
            "atl.general",
            "atl.admin",
            "atl.userprofile"
    );

    public WebResourceProperties() {
        this("My Web Resource");
    }

    public WebResourceProperties(String moduleName) {
        super(moduleName);
        setDependencies(new ArrayList<>());
        setContexts(new ArrayList<>());
        setTransformations(new ArrayList<>());
        setResources(new ArrayList<>());
    }

    public void setResources(List<Resource> resources) {
        put(RESOURCES, resources);
    }

    public void addResource(Resource resource) {
        getResources().add(resource);
    }

    public List<Resource> getResources() {
        //noinspection unchecked
        return (List<Resource>) get(RESOURCES);
    }

    public void setDependencies(List<String> dependencies) {
        put(DEPENDENCIES, dependencies);
    }

    public List<String> getDependencies() {
        //noinspection unchecked
        return (List<String>) get(DEPENDENCIES);
    }

    @SuppressWarnings(value = "unchecked")
    public void addDependency(String dependency) {
        List<String> dependencies = (List<String>) get(DEPENDENCIES);
        if (dependencies == null) {
            dependencies = new ArrayList<>();
            setDependencies(dependencies);
        }

        dependencies.add(dependency);
    }

    public void setContexts(List<String> contexts) {
        put(CONTEXTS, contexts);
    }

    public List<String> getContexts() {
        //noinspection unchecked
        return (List<String>) get(CONTEXTS);
    }

    @SuppressWarnings(value = "unchecked")
    public void addContext(String context) {
        List<String> contexts = (List<String>) get(CONTEXTS);
        if (contexts == null) {
            contexts = new ArrayList<>();
            setContexts(contexts);
        }

        contexts.add(context);
    }

    public void setTransformations(List<WebResourceTransformation> transformations) {
        put(TRANSFORMATIONS, transformations);
    }

    public List<WebResourceTransformation> getTransformations() {
        //noinspection unchecked
        return (List<WebResourceTransformation>) get(TRANSFORMATIONS);
    }

    @SuppressWarnings(value = "unchecked")
    public void addTransformation(WebResourceTransformation transformation) {
        List<WebResourceTransformation> transformations = (List<WebResourceTransformation>) get(TRANSFORMATIONS);
        if (transformations == null) {
            transformations = new ArrayList<>();
            setTransformations(transformations);
        }

        transformations.add(transformation);
    }

    public List<String> knownContexts() {
        return unmodifiableList(KNOWN_CONTEXTS);
    }
}
