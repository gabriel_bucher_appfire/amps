package com.atlassian.plugins.codegen.util;

import org.dom4j.io.SAXReader;
import org.xml.sax.SAXException;

import javax.annotation.ParametersAreNonnullByDefault;

/**
 * Utility methods relating to Dom4j.
 *
 * @since 8.3
 */
@ParametersAreNonnullByDefault
public final class Dom4jUtil {

    /**
     * Returns a SAX reader with no access to external entities, for security reasons.
     *
     * @return a new instance
     */
    public static SAXReader newSaxReader() {
        final SAXReader reader = new SAXReader();
        try {
            reader.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
            return reader;
        } catch (SAXException e) {
            throw new IllegalStateException(e);
        }
    }

    private Dom4jUtil() {}
}
