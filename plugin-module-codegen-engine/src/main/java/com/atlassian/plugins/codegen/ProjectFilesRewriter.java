package com.atlassian.plugins.codegen;

import com.atlassian.plugins.codegen.modules.PluginModuleLocation;
import com.atlassian.plugins.codegen.util.PluginXmlHelper;
import org.dom4j.DocumentException;

import javax.annotation.Nonnull;
import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.util.Properties;

import static com.atlassian.plugins.codegen.SourceFile.SourceGroup.TESTS;
import static com.atlassian.plugins.codegen.util.FileUtil.dotDelimitedFilePath;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.io.Files.createParentDirs;
import static java.nio.charset.StandardCharsets.UTF_8;
import static org.apache.commons.io.FileUtils.readFileToString;
import static org.apache.commons.io.FileUtils.writeByteArrayToFile;
import static org.apache.commons.io.FileUtils.writeStringToFile;

/**
 * Applies the changes from a {@link PluginProjectChangeset} that involve creating
 * source or resource files, or adding i18n strings.
 */
public class ProjectFilesRewriter implements ProjectRewriter {

    private final PluginModuleLocation location;

    public ProjectFilesRewriter(final PluginModuleLocation location) {
        this.location = checkNotNull(location, "location");
    }

    @Override
    public void applyChanges(final PluginProjectChangeset changes) throws Exception {
        createSourceFiles(changes);
        createResourceFiles(changes);
        if (changes.hasItems(I18nString.class)) {
            addI18nStrings(changes.getItems(I18nString.class));
        }
    }

    private void createSourceFiles(final PluginProjectChangeset changes) throws IOException {
        for (SourceFile sourceFile : changes.getItems(SourceFile.class)) {
            final File baseDir =
                    sourceFile.getSourceGroup() == TESTS ? location.getTestDirectory() : location.getSourceDirectory();
            final File newFile = dotDelimitedFilePath(baseDir, sourceFile.getClassId().getFullName(), ".java");
            createParentDirs(newFile);
            writeStringToFile(newFile, sourceFile.getContent(), UTF_8);
        }
    }

    private void createResourceFiles(final PluginProjectChangeset changes) throws IOException {
        for (ResourceFile resourceFile : changes.getItems(ResourceFile.class)) {
            File resourceDir = location.getResourcesDir();
            if (!resourceFile.getRelativePath().equals("")) {
                resourceDir = new File(resourceDir, resourceFile.getRelativePath());
            }
            final File newFile = new File(resourceDir, resourceFile.getName());
            createParentDirs(newFile);
            writeByteArrayToFile(newFile, resourceFile.getContent());
        }
    }

    private void addI18nStrings(final Iterable<I18nString> i18nStrings) throws IOException, DocumentException {
        final File propertiesFile = getI18nFile();
        final String oldContent = propertiesFile.exists() ? readFileToString(propertiesFile, UTF_8) : "";
        final Properties oldProps = new Properties();
        oldProps.load(new StringReader(oldContent));
        final StringBuilder newContent = new StringBuilder(oldContent);
        boolean modified = false;
        for (final I18nString item : i18nStrings) {
            if (!oldProps.containsKey(item.getName())) {
                if (!modified) {
                    newContent.append("\n");
                }
                modified = true;
                newContent.append(item.getName()).append("=").append(item.getValue()).append("\n");
                oldProps.put(item.getName(), item.getValue());
            }
        }
        if (modified) {
            writeStringToFile(propertiesFile, newContent.toString(), UTF_8);
        }
    }

    @Nonnull
    private File getI18nFile() throws IOException, DocumentException {
        final String defaultI18nLocation = new PluginXmlHelper(location).getDefaultI18nLocation();
        final File propertiesFile = dotDelimitedFilePath(
                location.getResourcesDir(), defaultI18nLocation, ".properties");
        createParentDirs(propertiesFile);
        return propertiesFile;
    }
}
