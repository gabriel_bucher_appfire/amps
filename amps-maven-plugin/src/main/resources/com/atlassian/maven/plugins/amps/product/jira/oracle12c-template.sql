-- This script must be run as a user with the "SYSDBA" role
DECLARE
  v_count INTEGER := 0;
  v_sid VARCHAR2(20);
BEGIN
  SELECT SYS_CONTEXT('userenv','instance_name') INTO v_sid FROM DUAL;

  -- Ensure we're in the root container
  EXECUTE IMMEDIATE 'ALTER SESSION SET CONTAINER=CDB$ROOT';

  -- Configure the Data Pump directory
  EXECUTE IMMEDIATE q'{CREATE OR REPLACE DIRECTORY DATA_PUMP_DIR AS 'v_data_pump_dir'}';

  -- Does the product's pluggable DB exist?
  SELECT COUNT (1) INTO v_count FROM cdb_pdbs WHERE pdb_name = 'PRODUCT_PDB';
  IF v_count > 0
  THEN
    -- Yes, close and drop it
    EXECUTE IMMEDIATE 'ALTER PLUGGABLE DATABASE PRODUCT_PDB CLOSE';
    EXECUTE IMMEDIATE 'DROP PLUGGABLE DATABASE PRODUCT_PDB INCLUDING DATAFILES';
  END IF;

  -- [Re]create the product pluggable DB, switch to it, and open it
  EXECUTE IMMEDIATE 'CREATE PLUGGABLE DATABASE PRODUCT_PDB ' ||
                    'ADMIN USER product_dba IDENTIFIED BY product_dba ' ||
                    'FILE_NAME_CONVERT = (''/u01/app/oracle/oradata/' || v_sid || '/pdbseed/'',''/u01/app/oracle/oradata/' || v_sid || '/PRODUCT_PDB/'')';
  EXECUTE IMMEDIATE 'ALTER SESSION SET CONTAINER=PRODUCT_PDB';
  EXECUTE IMMEDIATE 'ALTER PLUGGABLE DATABASE OPEN';

  -- Create the product user/schema in the product DB
  EXECUTE IMMEDIATE 'CREATE USER v_product_user IDENTIFIED BY v_product_pwd';
  EXECUTE IMMEDIATE 'GRANT CONNECT, RESOURCE, IMP_FULL_DATABASE TO v_product_user';
  EXECUTE IMMEDIATE 'GRANT READ, WRITE ON DIRECTORY DATA_PUMP_DIR TO v_product_user';
END;