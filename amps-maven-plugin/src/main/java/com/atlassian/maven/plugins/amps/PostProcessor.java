package com.atlassian.maven.plugins.amps;

import org.apache.maven.model.Build;
import org.apache.maven.plugin.MojoExecutionException;

import javax.annotation.Nonnull;

/**
 * <p>
 *     Allow processing of files in the project after all resources have been
 *     generated and processed.
 * </p>
 * <p>
 *     These are meant to run after all post-process steps are run because several userland maven
 *     configurations will write additional javascript, soy, etc., to the build output directory
 *     that may not be a part of the project's configured resource directories.
 * </p>
 * @since 8.3
 */
public interface PostProcessor {
    /**
     * Process files discoverable through the maven project's build object.
     * @param build The maven project's build object.
     * @throws MojoExecutionException if the post-processor discovers something about the resources
     * that should be considered a build error.
     */
    void processProjectBuild(@Nonnull Build build) throws MojoExecutionException;
}
