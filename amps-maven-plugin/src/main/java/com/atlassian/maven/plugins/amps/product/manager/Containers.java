package com.atlassian.maven.plugins.amps.product.manager;

import com.google.common.collect.ImmutableMap;

import java.util.Map;

/**
 * The {@link Container}s that AMPS supports.
 *
 * @since 8.3 was previously in {@code com.atlassian.maven.plugins.amps.product.manager.WebAppManagerImpl}.
 */
public final class Containers {

    private static final String TOMCAT_GROUP_ID = "org.apache.tomcat";
    private static final String TOMCAT_ARTIFACT_ID = "apache-tomcat";
    private static final String TOMCAT_8_X = "tomcat8x";
    private static final String WINDOWS_X_64 = "windows-x64";

    private static final Map<String, Container> CONTAINERS_BY_ID = ImmutableMap.<String, Container>builder()
            .put("tomcat5x", new Container(
                    "tomcat5x", TOMCAT_GROUP_ID, TOMCAT_ARTIFACT_ID, "5.5.36"))
            .put("tomcat6x", new Container(
                    "tomcat6x", TOMCAT_GROUP_ID, TOMCAT_ARTIFACT_ID, "6.0.41"))
            .put("tomcat7x", new Container(
                    "tomcat7x", TOMCAT_GROUP_ID, TOMCAT_ARTIFACT_ID, "7.0.73-atlassian-hosted", WINDOWS_X_64))
            .put(TOMCAT_8_X, new Container(
                    TOMCAT_8_X, TOMCAT_GROUP_ID, TOMCAT_ARTIFACT_ID, "8.0.53-atlassian-hosted", WINDOWS_X_64))
            .put("tomcat85x", new Container(
                    TOMCAT_8_X, TOMCAT_GROUP_ID, TOMCAT_ARTIFACT_ID, "8.5.40-atlassian-hosted", WINDOWS_X_64))
            .put("tomcat85_6", new Container(
                    TOMCAT_8_X, TOMCAT_GROUP_ID, TOMCAT_ARTIFACT_ID, "8.5.6-atlassian-hosted", WINDOWS_X_64))
            .put("tomcat85_29", new Container(
                    TOMCAT_8_X, TOMCAT_GROUP_ID, TOMCAT_ARTIFACT_ID, "8.5.29-atlassian-hosted", WINDOWS_X_64))
            .put("tomcat85_32", new Container(
                    TOMCAT_8_X, TOMCAT_GROUP_ID, TOMCAT_ARTIFACT_ID, "8.5.32-atlassian-hosted", WINDOWS_X_64))
            .put("tomcat85_35", new Container(
                    TOMCAT_8_X, TOMCAT_GROUP_ID, TOMCAT_ARTIFACT_ID, "8.5.35-atlassian-hosted", WINDOWS_X_64))
            .put("tomcat9x", new Container(
                    "tomcat9x", TOMCAT_GROUP_ID, TOMCAT_ARTIFACT_ID, "9.0.11-atlassian-hosted", WINDOWS_X_64))
            .put("jetty6x", new Container("jetty6x"))
            .put("jetty7x", new Container("jetty7x"))
            .put("jetty8x", new Container("jetty8x"))
            .put("jetty9x", new Container("jetty9x"))
            .build();

    /**
     * Returns the container with the given ID.
     *
     * @param containerId the container ID
     * @return the container
     * @throws IllegalArgumentException if the ID is invalid
     */
    public static Container findContainer(final String containerId) {
        final Container container = CONTAINERS_BY_ID.get(containerId);
        if (container == null) {
            throw new IllegalArgumentException("Container " + containerId + " not supported");
        }
        return container;
    }

    private Containers() {}
}
