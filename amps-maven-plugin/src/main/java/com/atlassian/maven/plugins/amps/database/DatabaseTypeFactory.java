package com.atlassian.maven.plugins.amps.database;

import com.atlassian.maven.plugins.amps.DataSource;
import org.apache.maven.plugin.logging.Log;

import javax.annotation.Nonnull;
import java.util.Collection;
import java.util.Optional;

import static java.util.Arrays.asList;

/**
 * A factory for {@link DatabaseType} instances.
 */
public class DatabaseTypeFactory {

    private final Collection<DatabaseType> supportedDatabaseTypes;

    /**
     * Constructor.
     *
     * @param log the Maven logger
     */
    public DatabaseTypeFactory(@Nonnull final Log log) {
        this.supportedDatabaseTypes = asList(
                new Hsql(),
                new H2(),
                new MySQL(log),
                new Postgres(log),
                new MssqlJtds(log),
                new MssqlMicrosoft(log),
                new Oracle12c(log), // must remain listed before Oracle 10g, as it's more specific
                new Oracle10g(log)
        );
    }

    /**
     * Returns the {@link DatabaseType} for the given {@link DataSource}.
     *
     * @param dataSource the data source
     * @return empty if the database can't be obtained
     */
    @Nonnull
    public Optional<DatabaseType> getDatabaseType(final DataSource dataSource) {
        return supportedDatabaseTypes.stream()
                .filter(db -> db.isTypeOf(dataSource))
                .findFirst();
    }
}
