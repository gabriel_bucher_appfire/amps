package com.atlassian.maven.plugins.amps.i18n;

import javax.annotation.Nonnull;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.atlassian.maven.plugins.amps.util.FileUtils.readFileToString;
import static java.nio.charset.StandardCharsets.UTF_8;
import static java.util.Collections.emptyList;

/**
 * Detects usages of i18n keys in javascript files.
 *
 * @since 8.3
 */
class FindI18nUsageInJavascript implements I18nScanner {
    private static final String I18N_KEY_GROUP = "i18nKey";

    // Not a very sophisticated matcher. Doesn't check
    // for variables after the key if a comma has been detected.
    private static final Pattern I18N_GET_TEXT_PATTERN = Pattern.compile(
            // PLUGWEB-422: Removed matching on identifiers because it would kill regex performance (up to 20x)
            "(?:\\.I18n|\\['I18n']|\\[\"I18n\"])" + // PLUGWEB-454: Babel 6 and Babel 7 + Webpack 4 uses bracket notation
                    "\\.getText" +
                    "\\(\\s*" + // open "function"
                    "(['\"])(?<" + I18N_KEY_GROUP + ">[\\w.-]+)\\1" + // single or double quoted word
                    "\\s*([),])" // close of "function", or ',' character for formatting argument, whatever is first
    );

    // PERF NOTE: Matching for long identifiers is about ~5% of time taken, another way to solve this is by applying
    // this transform to individual files rather than a batch, likewise if this was re-written to taken advantage of the
    // semantics of newlines (although that advantage is diminishing with minifiers removing new lines)
    private static final int LONGEST_ACCEPTED_IDENTIFIER = 120;
    // These two are directly linked  1 starting char + 100 char + 12 ["default"] + 8 .default
    private static final Pattern SINGLE_IDENTIFIER_PATTERN = Pattern.compile("(" +
            // "identifiers can contain only alphanumeric characters (or "$" or "_"), and may not start
            // with a digit" https://developer.mozilla.org/en-US/docs/Glossary/Identifier
            "(?:\\p{Alpha}|[$_])(?:\\p{Alnum}|[$_]){0,100}" +
            // optional ["default"] for babel 5, see PLUGWEB-306
            "(?:\\[['\"]default['\"]])?" +
            // optional .default for babel 6
            "(?:\\.default)?" +
            ")$");

    @Override
    @Nonnull
    public List<String> findI18nUsages(@Nonnull final File file) {
        return readFileToString(file, UTF_8).map(input -> {
            final List<String> usedI18nKeys = new ArrayList<>();

            // Matches the `I18n.getText(..` bit
            final Matcher i18nMatcher = I18N_GET_TEXT_PATTERN.matcher(input);
            // Matches a single identifier in front of `I18n.getText(`
            final Matcher singleIdentifierMatcher = SINGLE_IDENTIFIER_PATTERN.matcher(input);

            // PERF NOTE: Just finding the next `I18n.getText(..` accounts for about 25% of time taken.
            while (i18nMatcher.find()) {
                getValidI18nGetTextMatch(i18nMatcher, singleIdentifierMatcher).ifPresent(usedI18nKeys::add);
            }

            return usedI18nKeys;
        }).orElse(emptyList());
    }

    /**
     * Finds valid `I18n.getText(...)` calls and returns the i18n key used in it.
     * Behaviour is related to web resource manager's JsI18nTransformer and what it is capable of replacing at runtime;
     * this method will only return i18n keys preceded by a simple JavaScript identifier token.
     *
     * @param i18nMatcher the "primary" matcher that finds a `I18n.getText` call
     * @param singleIdentifierMatcher a "secondary" matcher that can find basic identifiers.
     * @return a String if the getText call was valid and replace-able at product runtime,
     *         or an empty option if the call would not be replace-able at product runtime.
     */
    @Nonnull
    private static Optional<String> getValidI18nGetTextMatch(final Matcher i18nMatcher, final Matcher singleIdentifierMatcher) {
        // Find where the `I18n.getText(...` call begins
        final int start = i18nMatcher.toMatchResult().start();
        // Limit the single identifier matcher's search space to the content just before the `I18n.getText(..` call
        singleIdentifierMatcher.region(
                Math.max(0, start - LONGEST_ACCEPTED_IDENTIFIER),
                // Limit the end to where `I18n.getText(` starts so the matcher works
                start);

        // PERF NOTE: Consumes about ~5% of time [checking to make sure it's safe to replace]
        // Looks for an identifier in front of `I18n.getText(`.
        if (!singleIdentifierMatcher.find()) {
            // Do nothing, we are going to play it safe and not replace anything since there was
            // nothing before the `I18n.getText.....` call because we expect it to be on the AJS
            // or WRM global objects (or renamed because of bundlers).
            return Optional.empty();
        }

        // It doesn't matter if it will be used in WRM.format or WRM.I18n.getText,
        // we can assume the match is for an i18n usage now.
        //
        // Get the i18n key out of the matched JS code.
        return Optional.of(i18nMatcher.group(I18N_KEY_GROUP));
    }
}
