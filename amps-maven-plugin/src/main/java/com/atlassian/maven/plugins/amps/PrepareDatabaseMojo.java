package com.atlassian.maven.plugins.amps;

import com.atlassian.maven.plugins.amps.database.DatabaseType;
import com.atlassian.maven.plugins.amps.database.DatabaseTypeFactory;
import com.atlassian.maven.plugins.amps.product.ImportMethod;
import com.google.common.annotations.VisibleForTesting;
import org.apache.maven.model.Dependency;
import org.apache.maven.model.Plugin;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.codehaus.plexus.util.xml.Xpp3Dom;

import javax.annotation.Nullable;
import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static com.atlassian.maven.plugins.amps.product.ImportMethod.SQL;
import static com.atlassian.maven.plugins.amps.product.ProductHandlerFactory.JIRA;
import static java.lang.String.format;
import static java.util.stream.Collectors.toList;
import static org.apache.commons.lang3.StringUtils.isNotEmpty;
import static org.apache.maven.plugins.annotations.ResolutionScope.TEST;
import static org.twdata.maven.mojoexecutor.MojoExecutor.goal;

/**
 * For each product instance with exactly one {@link DataSource} configured:
 * <ol>
 *     <li>Optionally drops and re-creates the database, then</li>
 *     <li>If a dump file is configured, imports it using either direct SQL or a db-specific import tool.</li>
 * </ol>
 */
@Mojo(name = "prepare-database", requiresDependencyResolution = TEST)
public class PrepareDatabaseMojo extends AbstractTestGroupsHandlerMojo {

    /**
     * If {@code true}, this goal does nothing. Synonymous with {@link #skipTests}.
     */
    @Parameter(property = "maven.test.skip", defaultValue = "false")
    private boolean testsSkip;

    /**
     * If {@code true}, this goal does nothing. Synonymous with {@link #testsSkip}.
     */
    @Parameter(property = "skipTests", defaultValue = "false")
    private boolean skipTests;

    /**
     * The path of a file to be imported into the database. If blank, no such import is performed.
     * Otherwise, this file needs to have the correct format for the configured {@link #importMethod}.
     */
    @Parameter(property = "db.dump.file.path")
    private String dumpFilePath;

    /**
     * The method by which the file indicated by {@link #dumpFilePath} should be imported. Valid values are:
     * <ul>
     *     <li>sql - execute the raw SQL statements contained in the dump file (the default)</li>
     *     <li>psql - pass the dump file to Postgres' PSQL command-line tool</li>
     *     <li>impdp - pass the dump file to Oracle's Import Data Pump tool</li>
     *     <li>sqlcmd - pass the dump file to Microsoft's sqlcmd command-line tool for SQL Server</li>
     * </ul>
     */
    @Parameter(property = "import.method")
    private String importMethod;

    /**
     * The JDBC URL of the default or "system" database.
     */
    @Parameter(property = "db.default.database")
    private String defaultDatabase;

    /**
     * The system (DBA) username. This user should have the privileges to drop and create a database.
     */
    @Parameter(property = "db.system.username")
    private String systemUsername;

    /**
     * The system (DBA) password.
     */
    @Parameter(property = "db.system.password")
    private String systemPassword;

    @Override
    protected void doExecute() throws MojoExecutionException {
        if (testsSkip || skipTests) {
            getLog().info("Database preparation skipped");
            return;
        }
        for (final Product product : getProductsToExecute()) {
            final DataSource dataSource = getOnlyDataSource(product);
            if (dataSource != null) {
                final DatabaseType databaseType = getDatabaseType(dataSource);
                dataSource.getLibArtifacts().addAll(getJdbcDriverArtifacts(product));
                if (dataSource.getLibArtifacts().isEmpty()) {
                    throw new MojoExecutionException("Neither the DataSource nor the Product have library artifacts," +
                            " no JDBC driver found for database: " + databaseType.getOfBizName());
                }
                populateParameters(dataSource);
                if (shouldDropAndReCreateDatabase(dataSource, product)) {
                    dropAndReCreate(databaseType, dataSource);
                }
                if (isNotEmpty(dataSource.getDumpFilePath())) {
                    importDumpFile(databaseType, dataSource);
                }
            }
        }
    }

    @VisibleForTesting
    static boolean shouldDropAndReCreateDatabase(final DataSource dataSource, final Product product) {
        // Originally, we always dropped and re-created the database for Jira, but not for any other products. Now we
        // have added the ability to drop and re-create the database for any product, but we can't make that the default
        // for all products, because users' non-Jira databases would suddenly start being dropped, which would surprise
        // them, to say the least. So we added the "dropAndReCreateDatabase" flag, defaulted to false, to allow users to
        // opt into that behaviour for the non-Jira products.
        //
        // However to preserve the existing behaviour of this mojo for Jira, we have to ignore that new parameter,
        // otherwise its default value of false would prevent the database being dropped and re-created as expected.
        return dataSource.isDropAndReCreateDatabase() || product.is(JIRA);
    }

    @Nullable
    private DataSource getOnlyDataSource(final Product product) {
        final List<DataSource> dataSources = product.getDataSources();
        switch (dataSources.size()) {
            case 0:
                getLog().info("No dataSource configured; skipping database preparation");
                return null;
            case 1:
                return dataSources.get(0);
            default:
                getLog().info("Multiple dataSources not supported. Configuration has these " +
                        dataSources.size() + " dataSources:");
                for (DataSource dataSource : dataSources) {
                    getLog().info("- Database URL: " + dataSource.getUrl());
                }
                return null;
        }
    }

    private DatabaseType getDatabaseType(final DataSource dataSource)
            throws MojoExecutionException {
        return new DatabaseTypeFactory(getLog()).getDatabaseType(dataSource)
                .orElseThrow(() -> new MojoExecutionException(
                        "Could not detect database type for dataSource: " + dataSource));
    }

    private Collection<LibArtifact> getJdbcDriverArtifacts(final Product product) {
        // Brute force approach: add all of the product's lib artifacts, and hope one of them is the JDBC driver
        return product.getLibArtifacts().stream()
                .map(this::toLibArtifact)
                .collect(toList());
    }

    private LibArtifact toLibArtifact(final ProductArtifact productArtifact) {
        return new LibArtifact(
                productArtifact.getGroupId(),
                productArtifact.getArtifactId(),
                productArtifact.getVersion()
        );
    }

    private void populateParameters(final DataSource dataSource) {
        if (isNotEmpty(defaultDatabase)) {
            dataSource.setSystemUrl(defaultDatabase);
        }
        if (isNotEmpty(systemUsername)) {
            dataSource.setSystemUsername(systemUsername);
        }
        if (isNotEmpty(systemPassword)) {
            dataSource.setSystemPassword(systemPassword);
        }
        if (isNotEmpty(dumpFilePath)) {
            dataSource.setDumpFilePath(dumpFilePath);
        }
        if (isNotEmpty(importMethod)) {
            dataSource.setImportMethod(importMethod);
        } else {
            // default is import standard sql
            dataSource.setImportMethod(SQL.getMethod());
        }
    }

    private void dropAndReCreate(final DatabaseType databaseType, final DataSource dataSource) throws MojoExecutionException {
        mojoExecutorWrapper.executeWithMergedConfig(
                getSqlMavenPlugin(databaseType, dataSource),
                goal("execute"),
                databaseType.getSqlMavenCreateConfiguration(dataSource),
                getMavenContext().getExecutionEnvironment()
        );
    }

    private Plugin getSqlMavenPlugin(final DatabaseType databaseType, final DataSource dataSource) {
        final Plugin sqlMavenPlugin =
                getMavenContext().getPlugin("org.codehaus.mojo", "sql-maven-plugin");
        final List<Dependency> pluginDependencies = new ArrayList<>(sqlMavenPlugin.getDependencies());
        pluginDependencies.addAll(databaseType.getSqlMavenDependencies(dataSource));
        sqlMavenPlugin.setDependencies(pluginDependencies);
        return sqlMavenPlugin;
    }

    private void importDumpFile(final DatabaseType databaseType, final DataSource dataSource)
            throws MojoExecutionException {
        final String dumpFileName = dataSource.getDumpFilePath();
        final File dumpFile = new File(dumpFileName);
        if (!dumpFile.isFile()) {
            throw new MojoExecutionException(format("Import file '%s' is not a file", dumpFileName));
        }
        getLog().info("Importing dump file: " + dumpFileName + " into " + databaseType.getOfBizName() +
                " using " + dataSource.getImportMethod());
        if (SQL == ImportMethod.getValueOf(dataSource.getImportMethod())) {
            importSqlDumpFile(databaseType, dataSource);
        } else {
            importDataUsingDatabaseSpecificTool(databaseType, dataSource);
        }
    }

    private void importSqlDumpFile(final DatabaseType databaseType, final DataSource dataSource)
            throws MojoExecutionException {
        mojoExecutorWrapper.executeWithMergedConfig(
                getSqlMavenPlugin(databaseType, dataSource),
                goal("execute"),
                databaseType.getSqlMavenFileImportConfiguration(dataSource),
                getMavenContext().getExecutionEnvironment()
        );
    }

    private void importDataUsingDatabaseSpecificTool(final DatabaseType databaseType, final DataSource dataSource)
            throws MojoExecutionException {
        final Xpp3Dom importConfiguration = databaseType.getExecMavenToolImportConfiguration(dataSource);
        if (importConfiguration == null) {
            getLog().warn(format("No configuration provided for %s - skipping import", databaseType));
        } else {
            mojoExecutorWrapper.executeWithMergedConfig(
                    getMavenContext().getPlugin("org.codehaus.mojo", "exec-maven-plugin"),
                    goal("exec"),
                    importConfiguration,
                    getMavenContext().getExecutionEnvironment()
            );
        }
    }
}
