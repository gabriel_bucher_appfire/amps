package com.atlassian.maven.plugins.amps;

import com.google.common.annotations.VisibleForTesting;

import javax.annotation.Nonnull;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.function.Supplier;

import static com.atlassian.maven.plugins.amps.util.ProductHandlerUtil.isPortFree;
import static com.atlassian.maven.plugins.amps.util.ProductHandlerUtil.pickFreePort;
import static java.lang.String.format;
import static java.util.Collections.unmodifiableMap;
import static org.apache.commons.lang3.StringUtils.defaultString;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

/**
 * A node of a {@link com.atlassian.maven.plugins.amps.Product} deployment.
 *
 * This is a DTO that Maven populates from the AMPS configuration in the user's POM. As such, it must:
 * <ul>
 *     <li>be located in the same package as the Mojo(s) that use it as a field type (either directly or in a
 *     collection), and</li>
 *     <li>have the same name (lower-cased) as the XML element to which it relates (i.e. {@code <node>})</li>
 * </ul>.
 *
 * @since 8.3
 */
public class Node {

    private static final String DEBUG_ARGS_FORMAT =
            " -Xdebug -Xrunjdwp:transport=dt_socket,address=%d,suspend=%s,server=y ";

    // Where applicable, we've named these the same as the fields in Product, to be more user-friendly
    @SuppressWarnings("MismatchedQueryAndUpdateOfCollection") // Maven populates this map from the user's POM
    private final Map<String, String> systemProperties = new HashMap<>();
    private Boolean jvmDebugSuspend;
    private boolean useHttps;
    private int ajpPort;
    private int httpPort;
    private int httpsPort;
    private int jvmDebugPort;
    private int rmiPort;
    private String debugArgs;

    // No-arg constructor for Maven to use when unmarshalling the user's POM
    public Node() {}

    // Constructor for synthesising a node when the user didn't provide one
    public Node(final boolean useHttps, final int ajpPort, final int jvmDebugPort, final int httpPort,
                final int httpsPort, final int rmiPort, final String debugArgs) {
        this.useHttps = useHttps;
        this.ajpPort = ajpPort;
        this.debugArgs = debugArgs;
        this.jvmDebugPort = jvmDebugPort;
        this.httpPort = httpPort;
        this.httpsPort = httpsPort;
        this.rmiPort = rmiPort;
    }

    public int getAjpPort() {
        return ajpPort;
    }

    public void setAjpPort(final int ajpPort) {
        this.ajpPort = ajpPort;
    }

    @Nonnull
    public String getDebugArgs() {
        return defaultString(debugArgs);
    }

    public void setDebugArgs(final String debugArgs) {
        this.debugArgs = debugArgs;
    }

    public int getJvmDebugPort() {
        return jvmDebugPort;
    }

    public void setJvmDebugPort(final int jvmDebugPort) {
        this.jvmDebugPort = jvmDebugPort;
    }

    public int getRmiPort() {
        return rmiPort;
    }

    public void setRmiPort(final int rmiPort) {
        this.rmiPort = rmiPort;
    }

    /**
     * Returns the user-configured system properties for this node.
     *
     * @return an unmodifiable copy of this map
     */
    @Nonnull
    public Map<String, String> getSystemProperties() {
        return unmodifiableMap(systemProperties);
    }

    /**
     * Returns the HTTP or HTTPS port of this node, depending on the value of {@link #useHttps}.
     *
     * @return see description
     */
    public int getWebPort() {
        return useHttps ? httpsPort : httpPort;
    }

    /**
     * Sets the HTTP or HTTPS port of this node, depending on the value of {@link #useHttps}.
     *
     * @param webPort the web port to set
     */
    public void setWebPort(final int webPort) {
        if (useHttps) {
            httpsPort = webPort;
        } else {
            httpPort = webPort;
        }
    }

    @VisibleForTesting
    void setJvmDebugSuspend(final boolean jvmDebugSuspend) {
        this.jvmDebugSuspend = jvmDebugSuspend;
    }

    /**
     * Sets the debug-related JVM arguments for this node, if they aren't already set (by the user in their POM).
     *
     * @param jvmDebugSuspend whether to suspend the JVM so that the user can attach their remote debugger
     * @throws IllegalStateException if this node's debug port has not been set to a non-zero value
     * @see #setJvmDebugPort(int)
     */
    public void defaultDebugArgs(final boolean jvmDebugSuspend) {
        if (isNotBlank(debugArgs)) {
            return; // no defaulting required
        }
        if (jvmDebugPort == 0) {
            throw new IllegalStateException("Cannot set debug args unless debug port is set");
        }
        final boolean suspend = Optional.ofNullable(this.jvmDebugSuspend).orElse(jvmDebugSuspend);
        final String suspendArg = suspend ? "y" : "n";
        setDebugArgs(format(DEBUG_ARGS_FORMAT, jvmDebugPort, suspendArg));
    }


    /**
     * Ensures that this node's non-debug ports are set to real (non-zero) values. This method is idempotent.
     *
     * @param instanceId the ID of the product instance to which this node belongs
     * @throws IllegalArgumentException if any user-configured port is not free
     * @since 8.3
     */
    public void ensureNonDebugPortsAreSet(final String instanceId) {
        if (ajpPort == 0) {
            setAjpPort(resolvePort(getAjpPort(), instanceId, "AJP"));
        }
        if (rmiPort == 0) {
            setRmiPort(resolvePort(getRmiPort(), instanceId, "RMI"));
        }
        if (getWebPort() == 0) {
            setWebPort(resolvePort(getWebPort(), instanceId, "HTTP(S)"));
        }
    }

    private int resolvePort(final int configuredPort, final String instanceId, final String purpose) {
        if (configuredPort == 0) {
            return pickFreePort(0);
        }
        if (isPortFree(configuredPort)) {
            return configuredPort;
        }
        throw new IllegalArgumentException(format(
                "%s: The configured %s port, %d, is in use", instanceId, purpose, configuredPort));
    }

    /**
     * If the given system property is not set for this node, this method sets it to the given value.
     *
     * @param key the name of the property
     * @param valueSupplier provides the value to set (if this key is not already set)
     * @see Map#computeIfAbsent
     * @see #setSystemProperty(String, String)
     */
    public void defaultSystemProperty(final String key, final Supplier<String> valueSupplier) {
        systemProperties.computeIfAbsent(key, k -> valueSupplier.get());
    }

    /**
     * Unconditionally sets the given system property to the given value for this node.
     *
     * @param key the name of the property
     * @param value the value to set
     * @see #defaultSystemProperty(String, Supplier)
     */
    public void setSystemProperty(final String key, final String value) {
        systemProperties.put(key, value);
    }
}
