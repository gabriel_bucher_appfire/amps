package com.atlassian.maven.plugins.amps.license;

import com.atlassian.maven.plugins.amps.Product;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.logging.Log;

import javax.annotation.ParametersAreNonnullByDefault;
import java.util.Optional;

import static java.lang.String.format;
import static java.util.Objects.requireNonNull;

/**
 * Installs a license into a running product. A typical use case would be replacing the Server license baked into the
 * product's data file with a Data Center license.
 *
 * @since 8.2
 */
@ParametersAreNonnullByDefault
public class LicenseInstaller {

    private final Log log;

    public LicenseInstaller(final Log log) {
        this.log = requireNonNull(log);
    }

    /**
     * Installs the license contained within the given product, if any, replacing any currently installed product
     * license, e.g. that provided by the product's data file.
     *
     * @param product the product, possibly containing a license to be installed
     * @param webPort the web port of the running product (for REST calls); we don't simply use the port in the given
     *                product, because that might not be the port the product is running on (e.g. might be zero)
     * @throws MojoExecutionException if the installation fails
     */
    public void installLicense(final Product product, final int webPort) throws MojoExecutionException {
        final Optional<String> license = product.getUserConfiguredLicense();
        if (license.isPresent()) { // no ifPresent lambda, because exception thrown below
            final LicenseBackdoorClient licenseBackdoorClient = new LicenseBackdoorClient(
                    webPort, product.getProtocol(), product.getServer(), product.getContextPath(), log);
            if (!licenseBackdoorClient.installProductLicense(license.get())) {
                throw new MojoExecutionException(format("Could not install %s license", product.getId()));
            }
            log.info(format("Applied provided license to %s", product.getId()));
        }
    }
}
