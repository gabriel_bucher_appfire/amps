package com.atlassian.maven.plugins.amps.osgi;

import aQute.bnd.osgi.Constants;
import com.atlassian.maven.plugins.amps.AbstractAmpsMojo;
import com.google.common.collect.ImmutableMap;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static aQute.bnd.osgi.Constants.NOEE;
import static com.atlassian.maven.plugins.amps.util.FileUtils.file;

/**
 * Generates an OSGi manifest for the plugin.
 */
@Mojo(name = "generate-manifest")
public class GenerateManifestMojo extends AbstractAmpsMojo {
    private static final String BUILD_DATE_ATTRIBUTE = "Atlassian-Build-Date";

    // Non-static to reduce chances of thread-safety issues (SQ)
    private final DateFormat buildDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");

    /**
     * The BND instructions for the bundle.
     */
    @Parameter
    @SuppressWarnings("FieldMayBeFinal") // being final might break Mojo parameter injection
    private Map<String, String> instructions = new HashMap<>();

    public void execute() throws MojoExecutionException, MojoFailureException {
        final MavenProject project = getMavenContext().getProject();

        // The Atlassian-Build-Date manifest attribute is used by the Atlassian licensing framework to determine
        // chronological order of bundle versions.
        final String buildDateStr = buildDateFormat.format(new Date());
        final Map<String, String> basicAttributes = ImmutableMap.of(BUILD_DATE_ATTRIBUTE, buildDateStr);

        if (!instructions.isEmpty()) {
            getLog().info("Generating a manifest for this plugin");

            if (!instructions.containsKey(Constants.EXPORT_PACKAGE)) {
                instructions.put(Constants.EXPORT_PACKAGE, "");
            }

            final File metaInfLib = file(project.getBuild().getOutputDirectory(), "META-INF", "lib");

            if (!instructions.containsKey(NOEE)) {
                instructions.put(NOEE, Boolean.TRUE.toString());
            }

            if (metaInfLib.exists()) {
                final StringBuilder classpath = new StringBuilder(".");
                final File[] metaInfFiles = metaInfLib.listFiles();
                if (metaInfFiles != null) {
                    for (File lib : metaInfFiles) {
                        classpath.append(",META-INF/lib/").append(lib.getName());
                    }
                }
                instructions.put(Constants.BUNDLE_CLASSPATH, classpath.toString());
            }
            getMavenGoals().generateBundleManifest(instructions, basicAttributes);
        } else {
            getMavenGoals().generateMinimalManifest(basicAttributes);
        }
    }
}
