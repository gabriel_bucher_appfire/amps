package com.atlassian.maven.plugins.amps.product;

import com.atlassian.maven.plugins.amps.MavenContext;
import com.atlassian.maven.plugins.amps.MavenGoals;
import com.atlassian.maven.plugins.amps.Product;
import com.atlassian.maven.plugins.amps.ProductArtifact;
import com.atlassian.maven.plugins.amps.product.manager.WebAppManager;
import com.atlassian.maven.plugins.amps.util.JvmArgsFix;
import com.atlassian.maven.plugins.amps.util.VersionUtils;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.collect.ImmutableMap;
import org.apache.maven.artifact.resolver.ArtifactResolver;
import org.apache.maven.repository.RepositorySystem;

import javax.annotation.Nonnull;
import java.io.File;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static java.lang.String.format;
import static java.util.Collections.emptyList;
import static java.util.Collections.emptyMap;
import static java.util.Optional.empty;
import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

/**
 * The {@link ProductHandler} for the Atlassian Reference Application (Refapp).
 */
public class RefappProductHandler extends AbstractWebappProductHandler {

    @VisibleForTesting
    static final String ATLASSIAN_BUNDLED_PLUGINS_ZIP = "WEB-INF/classes/atlassian-bundled-plugins.zip";
    @VisibleForTesting
    static final String ATLASSIAN_BUNDLED_PLUGINS_DIR = "WEB-INF/atlassian-bundled-plugins";

    public RefappProductHandler(final MavenContext context, final MavenGoals goals,
                                final RepositorySystem repositorySystem, final ArtifactResolver artifactResolver,
                                final WebAppManager webAppManager) {
        super(context, goals, new RefappPluginProvider(), repositorySystem, artifactResolver, webAppManager);
    }

    @Nonnull
    @Override
    public String getDefaultContainerId() {
        return "tomcat7x";
    }

    @Override
    @Nonnull
    public String getId() {
        return "refapp";
    }

    @Override
    public int getDefaultHttpPort() {
        return 5990;
    }

    @Override
    public int getDefaultHttpsPort() {
        return 8445;
    }

    @Nonnull
    @Override
    protected Optional<File> getUserInstalledPluginsDirectory(final Product product, final File webappDir, File homeDir) {
        return empty();
    }

    @Nonnull
    @Override
    protected File getBundledPluginPath(final Product product, final File productDir) {
        // if the bundled plugin directory exists, use it, otherwise fallback to the old zip behaviour.
        final File bundleDir = new File(productDir, ATLASSIAN_BUNDLED_PLUGINS_DIR);

        if (bundleDir.exists() && bundleDir.isDirectory()) {
            return bundleDir;
        }
        return new File(productDir, ATLASSIAN_BUNDLED_PLUGINS_ZIP);
    }

    @Override
    @Nonnull
    protected Map<String, String> getProductSpecificSystemProperties(final Product product, final int nodeIndex) {
        final ImmutableMap.Builder<String, String> properties = ImmutableMap.builder();
        properties.put("refapp.home", getHomeDirectory(product).getPath());
        properties.put("osgi.cache", getHomeDirectory(product).getPath() + "/osgi-cache");
        properties.put("bundledplugins.cache", getHomeDirectory(product).getPath() + "/bundled-plugins");
        properties.put("cargo.servlet.uriencoding", "UTF-8");
        properties.putAll(getExternalDatabaseSystemProperties(product));
        return properties.build();
    }

    private File getHomeDirectory(final Product product) {
        final List<File> homeDirectories = getHomeDirectories(product);
        if (homeDirectories.size() == 1) {
            return homeDirectories.get(0);
        }
        // Refapp doesn't support multi-node
        throw new IllegalStateException("Expected one home directory but found " + homeDirectories);
    }

    /**
     * Returns the system properties that Refapp expects when connecting to an external database.
     *
     * @param refapp the Refapp instance being started
     * @return a non-null map
     * @since 8.3
     */
    @Nonnull
    private Map<String, String> getExternalDatabaseSystemProperties(final Product refapp) {
        return refapp.getDataSources().stream().findFirst().map(ds -> {
            final ImmutableMap.Builder<String, String> externalDbProperties = ImmutableMap.builder();
            // Enable the external database
            externalDbProperties.put("refapp.jdbc.external", "true");
            // Mandatory properties, according to Refapp's ConnectionProviderImpl
            externalDbProperties.put("refapp.jdbc.app.url", nonBlankValue(ds.getUrl(), "url"));
            externalDbProperties.put("refapp.jdbc.app.user", nonBlankValue(ds.getUsername(), "username"));
            externalDbProperties.put("refapp.jdbc.app.pass", nonBlankValue(ds.getPassword(), "password"));
            externalDbProperties.put("refapp.jdbc.driver.class.name", nonBlankValue(ds.getDriver(), "driver"));
            // Optional properties, according to Refapp's ConnectionProviderImpl
            if (isNotBlank(ds.getSchema())) {
                externalDbProperties.put("refapp.jdbc.app.schema", ds.getSchema());
            }
            if (isNotBlank(ds.getValidationQuery())) {
                externalDbProperties.put("refapp.jdbc.validation.query", ds.getValidationQuery());
            }
            return (Map<String, String>) externalDbProperties.build();
        }).orElse(emptyMap());
    }

    private static String nonBlankValue(final String value, final String name) {
        if (isBlank(value)) {
            throw new IllegalArgumentException(format("Invalid dataSource.%s value '%s'", name, value));
        }
        return value.trim();
    }

    @Nonnull
    @Override
    public ProductArtifact getArtifact() {
        return new ProductArtifact("com.atlassian.refapp", "atlassian-refapp", VersionUtils.getVersion());
    }

    @Nonnull
    @Override
    public Optional<ProductArtifact> getTestResourcesArtifact() {
        return empty();
    }

    private static class RefappPluginProvider extends AbstractPluginProvider {

        @Override
        protected Collection<ProductArtifact> getSalArtifacts(String salVersion) {
            return Arrays.asList(
                    new ProductArtifact("com.atlassian.sal", "sal-api", salVersion),
                    new ProductArtifact("com.atlassian.sal", "sal-refimpl-appproperties-plugin", salVersion),
                    new ProductArtifact("com.atlassian.sal", "sal-refimpl-component-plugin", salVersion),
                    new ProductArtifact("com.atlassian.sal", "sal-refimpl-executor-plugin", salVersion),
                    new ProductArtifact("com.atlassian.sal", "sal-refimpl-lifecycle-plugin", salVersion),
                    new ProductArtifact("com.atlassian.sal", "sal-refimpl-message-plugin", salVersion),
                    new ProductArtifact("com.atlassian.sal", "sal-refimpl-net-plugin", salVersion),
                    new ProductArtifact("com.atlassian.sal", "sal-refimpl-pluginsettings-plugin", salVersion),
                    new ProductArtifact("com.atlassian.sal", "sal-refimpl-project-plugin", salVersion),
                    new ProductArtifact("com.atlassian.sal", "sal-refimpl-search-plugin", salVersion),
                    new ProductArtifact("com.atlassian.sal", "sal-refimpl-transaction-plugin", salVersion),
                    new ProductArtifact("com.atlassian.sal", "sal-refimpl-upgrade-plugin", salVersion),
                    new ProductArtifact("com.atlassian.sal", "sal-refimpl-user-plugin", salVersion));
        }

        @Override
        protected Collection<ProductArtifact> getPdkInstallArtifacts(String pdkInstallVersion) {
            return emptyList();
        }
    }

    @Override
    protected void fixJvmArgs(Product product) {
        final String jvmArgs = JvmArgsFix.defaults()
                .withAddOpens(ADD_OPENS_FOR_TOMCAT)
                .withAddOpens(ADD_OPENS_FOR_FELIX)
                .apply(product.getJvmArgs());
        product.setJvmArgs(jvmArgs);
    }
}
