package com.atlassian.maven.plugins.amps.product.common;

import org.dom4j.Document;

import javax.annotation.ParametersAreNonnullByDefault;

/**
 * Validates an XML document.
 *
 * @since 8.2
 */
@ParametersAreNonnullByDefault
public interface XMLDocumentValidator {

    /**
     * Validates the given XML document.
     *
     * @param document the document to be validated
     * @throws ValidationException if validation fails
     */
    void validate(Document document) throws ValidationException;
}
