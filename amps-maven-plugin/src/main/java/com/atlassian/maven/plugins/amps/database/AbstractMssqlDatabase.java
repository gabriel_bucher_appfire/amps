package com.atlassian.maven.plugins.amps.database;

import com.atlassian.maven.plugins.amps.DataSource;
import com.atlassian.maven.plugins.amps.product.ImportMethod;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.logging.Log;
import org.codehaus.plexus.util.xml.Xpp3Dom;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.DriverPropertyInfo;
import java.sql.SQLException;

import static org.twdata.maven.mojoexecutor.MojoExecutor.configuration;
import static org.twdata.maven.mojoexecutor.MojoExecutor.element;
import static org.twdata.maven.mojoexecutor.MojoExecutor.name;

@ParametersAreNonnullByDefault
public abstract class AbstractMssqlDatabase extends AbstractDatabase {

    private static final String ARGUMENT = "argument";

    private static final String DROP_DATABASE =
            "USE [master]; \n"
                    + "IF EXISTS(SELECT * FROM SYS.DATABASES WHERE name='%s') \n"
                    + "DROP DATABASE [%s];\n";
    private static final String DROP_USER =
            "USE [master]; \n"
                    + "IF EXISTS(SELECT * FROM SYS.SERVER_PRINCIPALS WHERE name = '%s') \n"
                    + "DROP LOGIN %s; \n";
    private static final String CREATE_DATABASE =
            "USE [master]; \n "
                    + "CREATE DATABASE [%s]; \n";
    private static final String CREATE_USER =
            "USE [master]; \n "
                    + "CREATE LOGIN %s WITH PASSWORD = '%s'; \n";
    private static final String GRANT_PERMISSION =
            "USE [%s];\n"
                    + "CREATE USER %s FROM LOGIN %s; \n"
                    + "EXEC SP_ADDROLEMEMBER 'DB_OWNER', '%s'; \n"
                    + "ALTER LOGIN %s WITH DEFAULT_DATABASE = [%s]; \n";

    protected AbstractMssqlDatabase(final Log log, final String driverClass, final String urlPrefix) {
        super(log, true, "mssql", driverClass, urlPrefix);
    }

    @Override
    protected String dropDatabase(final DataSource dataSource) throws MojoExecutionException {
        final String databaseName = getDatabaseName(dataSource);
        return String.format(DROP_DATABASE, databaseName, databaseName);
    }

    @Override
    protected String dropUser(final DataSource dataSource) {
        final String username = dataSource.getUsername();
        return String.format(DROP_USER, username, username);
    }

    @Override
    protected String createDatabase(final DataSource dataSource) throws MojoExecutionException {
        return String.format(CREATE_DATABASE, getDatabaseName(dataSource));
    }

    @Override
    protected String createUser(final DataSource dataSource) {
        return String.format(CREATE_USER, dataSource.getUsername(), dataSource.getPassword());
    }

    @Override
    protected String grantPermissionForUser(final DataSource dataSource) throws MojoExecutionException {
        final String username = dataSource.getUsername();
        final String databaseName = getDatabaseName(dataSource);
        return String.format(GRANT_PERMISSION, databaseName, username, username, username, username, databaseName);
    }

    @Override
    public Xpp3Dom getExecMavenToolImportConfiguration(final DataSource dataSource) throws MojoExecutionException {
        Xpp3Dom configDatabaseTool = null;
        if (ImportMethod.SQLCMD.equals(ImportMethod.getValueOf(dataSource.getImportMethod()))) {
            final String databaseName = getDatabaseName(dataSource);
            final String restoreAndGrantPermission = "\"RESTORE DATABASE " + "[" + databaseName + "] FROM DISK='"
                    + dataSource.getDumpFilePath() + "' WITH REPLACE; " + grantPermissionForUser(dataSource) + " \"";
            log.info("MSSQL restore database and grant permission: " + restoreAndGrantPermission);
            configDatabaseTool = configuration(
                    element(name("executable"), "Sqlcmd"),
                    element(name("arguments"),
                            element(name(ARGUMENT), "-s"),
                            element(name(ARGUMENT), "localhost"),
                            element(name(ARGUMENT), "-Q"),
                            element(name(ARGUMENT), restoreAndGrantPermission)
                    )
            );
        }
        return configDatabaseTool;
    }

    /**
     * Reference jtds documentation url http://jtds.sourceforge.net/faq.html The URL format for jTDS is:
     * jdbc:jtds:<server_type>://<server>[:<port>][/<database>][;<property>=<value>[;...]]
     * <p>
     * For Microsoft's sql server driver url https://docs.microsoft.com/en-us/sql/connect/jdbc/building-the-connection-url format is:
     * jdbc:sqlserver://[<serverName>[\<instanceName>][:<portNumber>]][;<property>=<value>[;...]]
     * eg. jdbc:sqlserver://localhost:1433;databaseName=AdventureWorks;integratedSecurity=true; +
     *
     * @param dataSource the datasource
     * @return database name
     */
    @Override
    protected String getDatabaseName(final DataSource dataSource) throws MojoExecutionException {
        try {
            Class.forName(dataSource.getDriver());
        } catch (ClassNotFoundException e) {
            throw new MojoExecutionException("Could not load MSSQL database library to classpath");
        }
        try {
            Driver driver = DriverManager.getDriver(dataSource.getUrl());
            DriverPropertyInfo[] driverPropertyInfos = driver.getPropertyInfo(dataSource.getUrl(), null);
            if (null != driverPropertyInfos) {
                for (DriverPropertyInfo driverPropertyInfo : driverPropertyInfos) {
                    if ("DATABASENAME".equalsIgnoreCase(driverPropertyInfo.name)) {
                        return driverPropertyInfo.value;
                    }
                }
            }
        } catch (SQLException e) {
            throw new MojoExecutionException("Could not detect database name from url: " + dataSource.getUrl());
        }
        return null;
    }

    @Override
    @Nonnull
    public Xpp3Dom getSqlMavenCreateConfiguration(final DataSource dataSource) throws MojoExecutionException {
        final String sql = dropDatabase(dataSource) + dropUser(dataSource) + createDatabase(dataSource) +
                createUser(dataSource) + grantPermissionForUser(dataSource);
        log.info("MSSQL initialization database sql: " + sql);
        final Xpp3Dom pluginConfiguration = systemDatabaseConfiguration(dataSource);
        pluginConfiguration.addChild(
                element(name("sqlCommand"), sql).toDom()
        );
        return pluginConfiguration;
    }
}
