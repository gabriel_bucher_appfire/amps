package com.atlassian.maven.plugins.amps.util;

import org.apache.maven.artifact.Artifact;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.project.DefaultProjectBuildingRequest;
import org.apache.maven.project.MavenProject;
import org.apache.maven.project.ProjectBuilder;
import org.apache.maven.project.ProjectBuildingException;
import org.apache.maven.project.ProjectBuildingRequest;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import java.util.Optional;

@ParametersAreNonnullByDefault
public class MavenProjectLoader {
    /**
     * Loads the given Maven project.
     *
     * @param project
     * @param pomArtifact
     * @param projectBuilder the Maven project builder
     * @return the maven project for the given artifact and session, if it can be resolved
     * Optional.empty() otherwise
     */
    @Nonnull
    public Optional<MavenProject> loadMavenProject(
            final MavenProject project, final Artifact pomArtifact, final ProjectBuilder projectBuilder)
            throws MojoExecutionException {
        try {
            ProjectBuildingRequest request = new DefaultProjectBuildingRequest(project.getProjectBuildingRequest())
                    .setRemoteRepositories(project.getRemoteArtifactRepositories());
            return Optional.ofNullable(projectBuilder.build(pomArtifact, request).getProject());
        } catch (ProjectBuildingException e) {
            throw new MojoExecutionException(String.format("Could not build the MavenProject for %s", pomArtifact), e);
        }
    }
}
