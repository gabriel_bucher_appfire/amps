RESOURCE_DOC_XML = "resourcedoc.xml"

/**
 * Returns the directory in which the RestDocsGenerator outputs the files it generates.
 */
File getDestinationDirectory() {
    //noinspection GrUnresolvedAccess - comes from invoker plugin
    new File(basedir, "target/classes")
}

/**
 * Asserts that the given files exist in the {@code target/classes} directory.
 *
 * @param filenames the filenames to check
 */
void assertFilesExistInTargetClasses(final String... filenames) {
    final File classesDirectory = getDestinationDirectory()
    for (filename in filenames) {
        final File fileToCheck = new File(classesDirectory, filename)
        if (!fileToCheck.isFile()) {
            throw new AssertionError(
                    "No such file amps-maven-plugin/target/its/generateRestDocsTest/target/classes/" + filename)
        }
    }
}

/**
 * Asserts that the given REST resource class names appear in resourcedoc.xml.
 *
 * @param classNames the class names to check for
 */
void assertResourceDocXmlContainsRestResources(final String... classNames) {
    def resourceDocXml = new File(getDestinationDirectory(), RESOURCE_DOC_XML).text
    for (final className in classNames) {
        if (!resourceDocXml.contains(className)) {
            throw new AssertionError("No REST resource called " + className)
        }
    }
}

assertFilesExistInTargetClasses("application-doc.xml", "application-grammars.xml", RESOURCE_DOC_XML)

assertResourceDocXmlContainsRestResources(
        "com.example.rest.explicit.ResourceInExplicitPackage",
        "com.example.rest.nonexplicit.ResourceInNonExplicitPackage");
