// check that the wired test ran, by checking the build log
final File buildLog = new File((String) basedir, "build.log")
assert buildLog.isFile() : "Build log '" + buildLog + "' does not exist!"
assert buildLog.length() > 0 : buildLog + " is empty!"

// read the file, as per https://www.baeldung.com/groovy-file-read
//noinspection GrUnresolvedAccess
assert buildLog.text.contains(">>>>> it.com.atlassian.amps.MyWiredTest passed")
