package com.atlassian.maven.plugins.amps.product;

import com.atlassian.maven.plugins.amps.DataSource;
import com.atlassian.maven.plugins.amps.MavenContext;
import com.atlassian.maven.plugins.amps.MavenGoals;
import com.atlassian.maven.plugins.amps.Node;
import com.atlassian.maven.plugins.amps.Product;
import com.atlassian.maven.plugins.amps.product.manager.WebAppManager;
import com.google.common.collect.ImmutableMap;
import org.apache.maven.artifact.resolver.ArtifactResolver;
import org.apache.maven.model.Build;
import org.apache.maven.plugin.logging.Log;
import org.apache.maven.project.MavenProject;
import org.apache.maven.repository.RepositorySystem;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.junit.rules.TemporaryFolder;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import static com.atlassian.maven.plugins.amps.product.RefappProductHandler.ATLASSIAN_BUNDLED_PLUGINS_ZIP;
import static java.util.Arrays.stream;
import static java.util.Collections.emptyList;
import static java.util.Collections.emptyMap;
import static java.util.Collections.singletonList;
import static org.apache.commons.lang3.JavaVersion.JAVA_9;
import static org.apache.commons.lang3.SystemUtils.isJavaVersionAtLeast;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasEntry;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertFalse;
import static org.junit.Assume.assumeTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@SuppressWarnings("unchecked")
public class TestRefappProductHandler {

    private static final String JDBC_DRIVER_CLASS = "theDriver";
    private static final String JDBC_PASSWORD = "thePassword";
    private static final String JDBC_SCHEMA = "theSchema";
    private static final String JDBC_URL = "theUrl";
    private static final String JDBC_USER = "theUser";
    private static final String JDBC_VALIDATION_QUERY = "theValidationQuery";

    /*
       These properties and those in the next map down are defined in Refapp, see
       https://bitbucket.org/atlassian/atlassian-refapp/src/master/refapp-core/src/main/java/com/atlassian/plugin/refimpl/db/ConnectionProviderImpl.java
     */
    private static final Map<String, String> MANDATORY_JDBC_PROPERTIES = ImmutableMap.of(
            "refapp.jdbc.app.url", JDBC_URL,
            "refapp.jdbc.app.user", JDBC_USER,
            "refapp.jdbc.app.pass", JDBC_PASSWORD,
            "refapp.jdbc.driver.class.name", JDBC_DRIVER_CLASS,
            "refapp.jdbc.external", "true"
    );

    private static final Map<String, String> OPTIONAL_JDBC_PROPERTIES = ImmutableMap.of(
            "refapp.jdbc.app.schema", JDBC_SCHEMA,
            "refapp.jdbc.validation.query", JDBC_VALIDATION_QUERY
    );

    @Rule
    public final MethodRule mockitoRule = MockitoJUnit.rule();

    @Rule
    public final TemporaryFolder tempHome = new TemporaryFolder();

    @Mock
    private ArtifactResolver artifactResolver;

    @Mock
    private Build build;

    @Mock
    private MavenContext mavenContext;

    @Mock
    private MavenGoals mavenGoals;

    @Mock
    private MavenProject mavenProject;

    @Mock
    private Product product;

    @Mock
    private RepositorySystem repositorySystem;

    @Mock
    private WebAppManager webAppManager;

    // Can't use @InjectMocks because of method calls in the superclass' constructors
    private RefappProductHandler productHandler;

    @Before
    public void setUp() {
        final Log log = mock(Log.class);
        when(mavenContext.getLog()).thenReturn(log);
        when(mavenContext.getProject()).thenReturn(mavenProject);
        productHandler = new RefappProductHandler(
                mavenContext, mavenGoals, repositorySystem, artifactResolver, webAppManager);
    }

    @Test
    public void bundledPluginsLocationCorrectForDirectory() throws IOException {
        final File bundledPluginsDir = tempHome.newFolder(ATLASSIAN_BUNDLED_PLUGINS_ZIP.split("/"));
        assertBundledPluginPath(tempHome.getRoot(), bundledPluginsDir);
    }

    @Test
    public void bundledPluginsLocationCorrectForFallback() {
        final File bundledPluginsZip = new File(tempHome.getRoot(), ATLASSIAN_BUNDLED_PLUGINS_ZIP);
        assertBundledPluginPath(tempHome.getRoot(), bundledPluginsZip);
    }

    private void assertBundledPluginPath(final File appDir, final File expectedPath) {
        // Invoke
        final File bundledPluginPath = productHandler.getBundledPluginPath(product, appDir);

        // Check
        assertThat(bundledPluginPath, notNullValue());
        assertThat(expectedPath, equalTo(bundledPluginPath));
    }

    @Test
    public void getProductSpecificSystemProperties_whenNoDataSourceConfigured_shouldNotAddDbProperties() {
        final Map<String, String> systemProperties = assertDatabaseSystemProperties(emptyList(), emptyMap());
        assertFalse(systemProperties.keySet().stream()
                .anyMatch(key -> key.startsWith("refapp.jdbc.")));
    }

    @Test
    public void getProductSpecificSystemProperties_whenMinimalDataSourceConfigured_shouldAddDbProperties() {
        assertDatabaseSystemProperties(singletonList(mockDataSource(false)),
                MANDATORY_JDBC_PROPERTIES);
    }

    @Test
    public void getProductSpecificSystemProperties_whenMaximalDataSourceConfigured_shouldAddDbProperties() {
        assertDatabaseSystemProperties(singletonList(mockDataSource(true)),
                MANDATORY_JDBC_PROPERTIES, OPTIONAL_JDBC_PROPERTIES);
    }

    private static DataSource mockDataSource(final boolean withAdvancedOptions) {
        final DataSource dataSource = mock(DataSource.class);
        when(dataSource.getDriver()).thenReturn(JDBC_DRIVER_CLASS);
        when(dataSource.getUsername()).thenReturn(JDBC_USER);
        when(dataSource.getPassword()).thenReturn(JDBC_PASSWORD);
        when(dataSource.getUrl()).thenReturn(JDBC_URL);
        if (withAdvancedOptions) {
            when(dataSource.getSchema()).thenReturn(JDBC_SCHEMA);
            when(dataSource.getValidationQuery()).thenReturn(JDBC_VALIDATION_QUERY);
        } else {
            // Here we're testing that the production code ignores all types of blank value
            when(dataSource.getSchema()).thenReturn(null);
            when(dataSource.getValidationQuery()).thenReturn("");
        }
        return dataSource;
    }

    private Map<String, String> assertDatabaseSystemProperties(
            final List<DataSource> dataSources, final Map<String, String>... expectedDatabaseProperties) {
        // Arrange
        when(mavenProject.getBuild()).thenReturn(build);
        when(build.getDirectory()).thenReturn("theBuildDirectory");
        when(product.getDataHome()).thenReturn("theParent/theDataHome");
        when(product.getDataSources()).thenReturn(dataSources);
        when(product.getInstanceId()).thenReturn("theInstanceId");
        final Node node = mock(Node.class);
        when(product.getNodes()).thenReturn(singletonList(node));

        // Act
        final Map<String, String> systemProperties =
                productHandler.getProductSpecificSystemProperties(product, 0);

        // Assert
        stream(expectedDatabaseProperties)
                .flatMap(map -> map.entrySet().stream())
                .forEach(entry -> assertThat(systemProperties, hasEntry(entry.getKey(), entry.getValue())));
        return systemProperties;
    }

    @Test
    public void testFixJvmArgs() {
        assumeTrue(isJavaVersionAtLeast(JAVA_9));

        productHandler.fixJvmArgs(product);

        final ArgumentCaptor<String> captor = ArgumentCaptor.forClass(String.class);

        verify(product).setJvmArgs(captor.capture());

        assertThat(captor.getValue(), allOf(
                containsString("-Xmx512m"),
                containsString("--add-opens")));
    }
}
