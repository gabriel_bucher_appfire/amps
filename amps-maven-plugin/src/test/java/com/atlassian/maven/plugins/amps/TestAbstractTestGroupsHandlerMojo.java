package com.atlassian.maven.plugins.amps;

import org.apache.maven.plugin.MojoExecutionException;
import org.junit.Test;

import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.startsWith;
import static org.junit.Assert.assertThrows;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

public class TestAbstractTestGroupsHandlerMojo {

    @Test
    public void testValidatePortConfigurationPass() throws MojoExecutionException {
        final Product product = getProduct(
                "testProduct", false, 7101, 7100, 0, 7102);
        final Product product1 = getProduct(
                "testProduct1", false, 7201, 7200, 0, 7202);
        final Product product2 = getProduct(
                "testProduct2", false, 7301, 7300, 0, 7302);

        //No conflicts should be detected
        runValidationTest(product, product1, product2);
    }

    @Test
    public void testValidatePortConfigurationCatchesConflictsWithinSameProduct() {
        final Product product =
                getProduct("testProduct", false, 7100, 7100, 0, 7100);

        // The AJP and RMI ports conflict with the HTTP port
        final MojoExecutionException actualException =
                assertThrows(MojoExecutionException.class, () -> runValidationTest(product));
        assertThat(actualException.getMessage(), startsWith("2 port conflicts were detected between the 1 products"));
    }

    @Test
    public void testValidatePortConfigurationIgnoresHttpAndHttpsConflictWhenNotUsed() throws Exception {
        final Product product = getProduct(
                "testProduct", false, 0, 7100, 0, 0);
        final Product product1 = getProduct(
                "testProduct1", true, 0, 7100, 7000, 0);

        //Should not detect a conflict as http port of testProduct1 is not used
        runValidationTest(product, product1);
    }

    @Test
    public void testValidatePortConfigurationDetectConflicts() {
        final Product product = getProduct(
                "testProduct", false, 7300, 7100, 0, 7302);
        final Product product1 = getProduct(
                "testProduct1", false, 7301, 7100, 0, 7302);
        final Product product2 = getProduct(
                "testProduct2", true, 7301, 0, 7300, 7302);

        // Should detect and print errors for all conflicts before throwing an exception
        final MojoExecutionException actualException =
                assertThrows(MojoExecutionException.class, () -> runValidationTest(product, product1, product2));
        assertThat(actualException.getMessage(), startsWith("5 port conflicts were detected between the 3 products"));
    }

    private static Product getProduct(final String testProduct2, final boolean useHttps, final int ajpPort,
                                      final int httpPort, final int httpsPort, final int rmiPort) {
        final Product product = new Product();
        product.setInstanceId(testProduct2);
        final Node node = mock(Node.class);
        product.setNodes(singletonList(node));
        final int webPort = useHttps ? httpsPort : httpPort;
        when(node.getAjpPort()).thenReturn(ajpPort);
        when(node.getRmiPort()).thenReturn(rmiPort);
        when(node.getWebPort()).thenReturn(webPort);
        return product;
    }

    private void runValidationTest(final Product... products) throws MojoExecutionException {
        final AbstractTestGroupsHandlerMojo testGroupsHandlerMojo = spy(AbstractTestGroupsHandlerMojo.class);
        testGroupsHandlerMojo.validatePortConfiguration(asList(products));
    }
}