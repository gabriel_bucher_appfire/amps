package com.atlassian.maven.plugins.amps;

import org.junit.Test;

import static com.atlassian.maven.plugins.amps.product.ProductHandlerFactory.JIRA;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class PrepareDatabaseMojoTest {

    private static void assertDropAndReCreateDatabase(
            final boolean parameterValue, final boolean isJira, final boolean expectedResult) {
        // Arrange
        final DataSource dataSource = mock(DataSource.class);
        final Product product = mock(Product.class);
        when(dataSource.isDropAndReCreateDatabase()).thenReturn(parameterValue);
        when(product.is(JIRA)).thenReturn(isJira);

        // Act
        final boolean result = PrepareDatabaseMojo.shouldDropAndReCreateDatabase(dataSource, product);

        // Assert
        assertThat(result, is(expectedResult));
    }

    @Test
    public void shouldDropAndReCreateDatabase_whenNotJira_shouldRespectFalseParameter() {
        assertDropAndReCreateDatabase(false, false, false);
    }

    @Test
    public void shouldDropAndReCreateDatabase_whenNotJira_shouldRespectTrueParameter() {
        assertDropAndReCreateDatabase(true, false, true);
    }

    @Test
    public void shouldDropAndReCreateDatabase_whenJira_shouldIgnoreFalseParameter() {
        // Setting the parameter to false won't change anything because that's its default anyway
        assertDropAndReCreateDatabase(false, true, true);
    }

    @Test
    public void shouldDropAndReCreateDatabase_whenJira_shouldIgnoreTrueParameter() {
        // Setting the parameter to true won't change anything, because that's already the setting for Jira
        assertDropAndReCreateDatabase(true, true, true);
    }
}
