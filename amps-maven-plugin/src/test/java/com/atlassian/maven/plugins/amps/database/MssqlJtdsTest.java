package com.atlassian.maven.plugins.amps.database;

import org.codehaus.plexus.util.xml.Xpp3Dom;
import org.junit.Test;
import org.mockito.InjectMocks;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;

public class MssqlJtdsTest extends DatabaseTestCase<MssqlJtds> {

    private static final String JTDS_DRIVER = "net.sourceforge.jtds.jdbc.Driver";

    @InjectMocks
    private MssqlJtds database;

    @Override
    protected MssqlJtds getDatabase() {
        return database;
    }

    @Test
    public void getDatabaseName_whenNoOptionsInJtdsUrl_shouldReturnCorrectDbName() throws Exception {
        assertDatabaseName(JTDS_DRIVER, "jdbc:jtds:sqlserver://localhost:1433/ddd", "ddd");
    }

    @Test
    public void getDatabaseName_whenOptionsInJtdsUrl_shouldReturnCorrectDbName() throws Exception {
        assertDatabaseName(JTDS_DRIVER, "jdbc:jtds:sybase://127.0.0.1/eeee;autoCommit=false", "eeee");
    }

    @Test
    public void testCreateDatabaseSql() throws Exception {
        // expected result
        final String expectedSQLGenerated = "USE [master]; \n"
                + "IF EXISTS(SELECT * FROM SYS.DATABASES WHERE name='productdb') \n"
                + "DROP DATABASE [productdb];\n"
                + "USE [master]; \n"
                + "IF EXISTS(SELECT * FROM SYS.SERVER_PRINCIPALS WHERE name = 'product_user') \n"
                + "DROP LOGIN product_user; \n"
                + "USE [master]; \n"
                + " CREATE DATABASE [productdb]; \n"
                + "USE [master]; \n"
                + " CREATE LOGIN product_user WITH PASSWORD = 'product_pwd'; \n"
                + "USE [productdb];\n"
                + "CREATE USER product_user FROM LOGIN product_user; \n"
                + "EXEC SP_ADDROLEMEMBER 'DB_OWNER', 'product_user'; \n"
                + "ALTER LOGIN product_user WITH DEFAULT_DATABASE = [productdb]; \n";
        // setup
        when(dataSource.getDriver()).thenReturn(JTDS_DRIVER);
        when(dataSource.getPassword()).thenReturn("product_pwd");
        when(dataSource.getSystemUrl()).thenReturn("theSystemUrl");
        when(dataSource.getSystemUsername()).thenReturn("theSystemUser");
        when(dataSource.getUrl()).thenReturn("jdbc:jtds:sqlserver://localhost:1433/productdb");
        when(dataSource.getUsername()).thenReturn("product_user");

        // execute
        final Xpp3Dom sqlMavenCreateConfiguration = database.getSqlMavenCreateConfiguration(dataSource);

        // assert
        final String initDatabaseSQL = sqlMavenCreateConfiguration.getChild("sqlCommand").getValue();
        assertThat("Generated SQL should be: " + expectedSQLGenerated,
                initDatabaseSQL, is(expectedSQLGenerated));
    }

    @Test
    public void testImportConfiguration() throws Exception {
        // setup
        final String expectedSQLGenerated =
                "\"RESTORE DATABASE [productdb] FROM DISK='product_mssql_dump.bak' WITH REPLACE; USE [productdb];\n" +
                        "CREATE USER product_user FROM LOGIN product_user; \n" +
                        "EXEC SP_ADDROLEMEMBER 'DB_OWNER', 'product_user'; \n" +
                        "ALTER LOGIN product_user WITH DEFAULT_DATABASE = [productdb]; \n" +
                        " \"";
        when(dataSource.getUrl()).thenReturn("jdbc:jtds:sqlserver://localhost:1433/productdb");
        when(dataSource.getUsername()).thenReturn("product_user");
        when(dataSource.getDriver()).thenReturn("net.sourceforge.jtds.jdbc.Driver");
        when(dataSource.getImportMethod()).thenReturn("SQLCMD");
        when(dataSource.getDumpFilePath()).thenReturn("product_mssql_dump.bak");

        // execute
        final Xpp3Dom toolImportConfiguration = database.getExecMavenToolImportConfiguration(dataSource);

        // assert
        assertNotNull(toolImportConfiguration);
        final String configDatabaseSQL = toolImportConfiguration.getChild("arguments").getChild(3).getValue();
        assertThat("Generated SQL should be: " + expectedSQLGenerated,
                configDatabaseSQL, is(expectedSQLGenerated));
    }
}
