package com.atlassian.maven.plugins.amps.product.manager;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import javax.annotation.Nullable;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertThat;
import static org.junit.rules.ExpectedException.none;

public class BaseUrlUtilsTest {

    @Rule
    public final ExpectedException expectedException = none();

    @Test
    public void getBaseUrl_whenUsingPort80_shouldOmitThePortNumber() {
        // Act
        final String baseUrl = BaseUrlUtils.getBaseUrl("anyServer", 80, "anyPath");

        // Assert
        assertThat(baseUrl, not(containsString("80")));
    }

    @Test
    public void getBaseUrl_whenNotUsingPort80_shouldContainThePortNumber() {
        // Arrange
        final int actualWebPort = 42;

        // Act
        final String baseUrl = BaseUrlUtils.getBaseUrl("anyServer", actualWebPort, "anyPath");

        // Assert
        assertThat(baseUrl, containsString(":" + actualWebPort));
    }

    @Test
    public void getBaseUrl_whenServerStartsWithHttp_shouldIncludeThatScheme() {
        // Arrange
        final String server = "http://theServer";

        // Act
        final String baseUrl = BaseUrlUtils.getBaseUrl(server, 80, "anyPath");

        // Assert
        assertThat(baseUrl, is("http://theServer/anyPath"));
    }

    @Test
    public void getBaseUrl_whenServerDoesNotStartWithHttp_shouldApplyThatScheme() {
        // Arrange
        final String server = "theServer";

        // Act
        final String baseUrl = BaseUrlUtils.getBaseUrl(server, 80, "myPath");

        // Assert
        assertThat(baseUrl, is("http://theServer/myPath"));
    }

    @Test
    public void getBaseUrl_whenContextPathIsEmpty_shouldNotIncludeOne() {
        // Act
        final String baseUrl = BaseUrlUtils.getBaseUrl("theServer", 80, "");

        // Assert
        assertThat(baseUrl, is("http://theServer"));
    }

    @Test
    public void getBaseUrl_whenContextPathDoesNotStartWithSlash_shouldIncludeOne() {
        // Act
        final String baseUrl = BaseUrlUtils.getBaseUrl("theServer", 80, "thePath");

        // Assert
        assertThat(baseUrl, is("http://theServer/thePath"));
    }

    @Test
    public void getBaseUrl_whenContextPathIsNull_shouldOmitIt() {
        // Act
        final String baseUrl = BaseUrlUtils.getBaseUrl("theServer", 42, null);

        // Assert
        assertThat(baseUrl, is("http://theServer:42"));
    }

    @Test
    public void getBaseUrl_whenGivenNullServer_shouldRejectIt() {
        assertInvalidServer(null);
    }

    @Test
    public void getBaseUrl_whenGivenEmptyServer_shouldRejectIt() {
        assertInvalidServer("");
    }

    private void assertInvalidServer(@Nullable final String server) {
        // Arrange
        expectedException.expect(IllegalArgumentException.class);

        // Act
        BaseUrlUtils.getBaseUrl(server, 42, "thePath");
    }
}
