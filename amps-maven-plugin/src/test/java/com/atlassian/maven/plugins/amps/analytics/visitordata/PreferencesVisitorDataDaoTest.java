package com.atlassian.maven.plugins.amps.analytics.visitordata;

import com.dmurph.tracking.VisitorData;
import org.apache.maven.plugin.logging.Log;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;

import static com.atlassian.maven.plugins.amps.analytics.visitordata.PreferencesVisitorDataDao.PREF_NAME;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.sameInstance;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class PreferencesVisitorDataDaoTest {

    @Rule
    public final MethodRule mockitoRule = MockitoJUnit.rule();

    @Mock
    private Log mavenLogger;

    @Mock
    private VisitorDataMarshaller marshaller;

    @InjectMocks
    private PreferencesVisitorDataDao dao;

    @Test
    public void load_whenNothingSaved_shouldReturnNewVisitorData() throws BackingStoreException {
        // Arrange
        final Preferences persistenceNode = PreferencesVisitorDataDao.getPreferencesPersistenceNode();
        persistenceNode.remove(PREF_NAME);
        persistenceNode.flush();
        when(marshaller.marshal(any())).thenReturn("anything");

        // Act
        final VisitorData loadedVisitorData = dao.load();

        // Assert
        assertThat(loadedVisitorData.getVisits(), is(1));
    }

    @Test
    public void load_whenPrecededBySave_shouldReturnTheSavedData() {
        // Arrange
        final VisitorData visitorToSave = mock(VisitorData.class);
        final VisitorData visitorToReturn = mock(VisitorData.class);
        final String marshalledVisitor = "theMarshalledVisitor";
        when(marshaller.marshal(visitorToSave)).thenReturn(marshalledVisitor);
        when(marshaller.unmarshalAndUpdate(marshalledVisitor)).thenReturn(visitorToReturn);

        // Act
        dao.save(visitorToSave);
        final VisitorData actualVisitorOut = dao.load();

        // Assert
        assertThat(actualVisitorOut, sameInstance(visitorToReturn));
    }
}
