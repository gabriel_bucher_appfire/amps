package com.atlassian.maven.plugins.amps;

import org.apache.maven.model.Dependency;
import org.junit.Test;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class LibArtifactTest {

    @Test
    public void asDependency_shouldReturnDependencyWithSameGav() {
        // Arrange
        final String groupId = "theGroupId";
        final String artifactId = "theArtifactId";
        final String version = "theVersion";
        final LibArtifact libArtifact = new LibArtifact(groupId, artifactId, version);

        // Act
        final Dependency dependency = libArtifact.asDependency();

        // Assert
        assertThat(dependency.getGroupId(), is(groupId));
        assertThat(dependency.getArtifactId(), is(artifactId));
        assertThat(dependency.getVersion(), is(version));
    }
}
