package com.atlassian.maven.plugins.amps;

import org.apache.maven.execution.MavenSession;
import org.apache.maven.model.Plugin;
import org.apache.maven.model.PluginManagement;
import org.apache.maven.plugin.BuildPluginManager;
import org.apache.maven.plugin.logging.Log;
import org.apache.maven.project.MavenProject;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.Properties;

import static java.util.Collections.emptyList;
import static java.util.Collections.singleton;
import static java.util.Collections.singletonList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.isEmptyOrNullString;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

public class TestMavenContext {

    private static final String MANAGED_ARTIFACT_ID = "managed";
    @Rule
    public final MockitoRule mockitoRule = MockitoJUnit.rule();

    @Mock
    private BuildPluginManager buildPluginManager;

    @Mock
    private Log log;

    @Mock
    private MavenProject project;

    @Mock
    private MavenSession session;

    @Mock
    private PluginManagement pluginManagement;

    private MavenContext mavenContext;

    @Before
    public void setUp() {
        mavenContext = new MavenContext(project, emptyList(), session, buildPluginManager, log);
        when(project.getPluginManagement()).thenReturn(pluginManagement);
    }

    @Test
    public void shouldNotOverwriteBndLib() {
        assertContextDoesNotOverridePlugin("bndlib");
    }

    @Test
    public void shouldNotOverwriteBizAQuteBndlib() {
        assertContextDoesNotOverridePlugin("biz.aQute.bndlib");
    }

    private void assertContextDoesNotOverridePlugin(final String artifactId) {
        try {
            mavenContext.getPlugin("anything", artifactId);
            fail(artifactId + " should not be overridden.");
        } catch (final IllegalArgumentException e) {
            // No override => all good
        }
    }

    @Test
    public void getPlugin_whenAskedForCodehausExecPlugin_shouldReturnValidVersion() {
        // Arrange
        final String groupId = "org.codehaus.mojo";
        final String artifactId = "exec-maven-plugin";

        // Act
        final Plugin execPlugin = mavenContext.getPlugin(groupId, artifactId);

        // Assert
        assertThat(execPlugin.getGroupId(), is(groupId));
        assertThat(execPlugin.getArtifactId(), is(artifactId));
        assertThat(execPlugin.getVersion(), not(isEmptyOrNullString()));
    }

    @Test
    public void getVersionOverrides_whenOverrideSetForUnmanagedPlugin_shouldLogWarning() {
        // Arrange
        mavenContext.setVersionOverrides(singleton("unmanaged"));
        final Plugin managedPlugin = mockManagedPlugin("irrelevant");
        when(pluginManagement.getPlugins()).thenReturn(singletonList(managedPlugin));

        // Act
        final Properties versionOverrides = mavenContext.getVersionOverrides();

        // Assert
        assertTrue(versionOverrides.isEmpty());
        verify(log).warn("Plugin artifactId(s) defined in 'versionOverrides' parameter" +
                " but no associated entry found in <pluginManagement> section for [unmanaged]");
    }

    @Test
    public void getVersionOverrides_whenOverrideSetForManagedPlugin_shouldContainManagedVersion() {
        // Arrange
        final String managedVersion = "theManagedVersion";
        mavenContext.setVersionOverrides(singleton(MANAGED_ARTIFACT_ID));
        final Plugin managedPlugin = mockManagedPlugin(managedVersion);
        when(pluginManagement.getPlugins()).thenReturn(singletonList(managedPlugin));

        // Act
        final Properties versionOverrides = mavenContext.getVersionOverrides();

        // Assert
        assertThat(versionOverrides.getProperty(MANAGED_ARTIFACT_ID), is(managedVersion));
        verifyNoMoreInteractions(log);
    }

    private static Plugin mockManagedPlugin(final String version) {
        final Plugin plugin = mock(Plugin.class);
        when(plugin.getArtifactId()).thenReturn(MANAGED_ARTIFACT_ID);
        when(plugin.getVersion()).thenReturn(version);
        return plugin;
    }
}
