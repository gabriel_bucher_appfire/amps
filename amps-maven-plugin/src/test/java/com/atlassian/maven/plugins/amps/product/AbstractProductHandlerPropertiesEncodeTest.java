package com.atlassian.maven.plugins.amps.product;

import org.junit.Test;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

/**
 * The {@link AbstractProductHandler#propertiesEncode(String)} method seems to have a bug;
 * I've written tests for it below to document the current behaviour and to preserve it in
 * case it's not actually a bug.
 */
public class AbstractProductHandlerPropertiesEncodeTest {

    @Test
    public void propertiesEncode_whenInputIsNull_shouldReturnNull() {
        assertPropertiesEncode(null, null); // legit
    }

    @Test
    public void propertiesEncode_whenInputIsEmpty_shouldReturnEmpty() {
        assertPropertiesEncode("", ""); // legit
    }

    @Test
    public void propertiesEncode_whenInputContainsNothingSpecial_shouldReturnThatInput() {
        assertPropertiesEncode("foo", "foo"); // legit
    }

    @Test
    public void propertiesEncode_whenInputContainsOneColon_shouldEscapeIt() {
        // suspect - I would expect the output to be "foo\:bar"
        assertPropertiesEncode("foo:bar", "foo:bar");
    }

    @Test
    public void propertiesEncode_whenInputContainsTwoColons_shouldEscapeThem() {
        assertPropertiesEncode("foo:bar:baz", "foo:bar:baz"); // suspect
    }

    @Test
    public void propertiesEncode_whenInputContainsOneEquals_shouldEscapeIt() {
        // suspect - I would expect the output to be "foo\=bar"
        assertPropertiesEncode("foo=bar", "foo=bar");
    }

    @Test
    public void propertiesEncode_whenInputContainsTwoEquals_shouldEscapeThem() {
        assertPropertiesEncode("foo=bar=baz", "foo=bar=baz"); // suspect
    }

    @Test
    public void propertiesEncode_whenInputContainsTwoOfEach_shouldEscapeThem() {
        assertPropertiesEncode("foo=bar:baz=bat:yob=yoo", "foo=bar:baz=bat:yob=yoo"); // suspect
    }

    private static void assertPropertiesEncode(final String input, final String expectedOutput) {
        assertThat(AbstractProductHandler.propertiesEncode(input), is(expectedOutput));
    }
}
