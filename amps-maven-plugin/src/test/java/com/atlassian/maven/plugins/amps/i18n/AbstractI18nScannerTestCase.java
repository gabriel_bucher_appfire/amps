package com.atlassian.maven.plugins.amps.i18n;

import org.apache.commons.io.FileUtils;
import org.junit.Rule;
import org.junit.rules.TemporaryFolder;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.file.Path;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.Properties;

import static java.nio.charset.StandardCharsets.UTF_8;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;
import static org.codehaus.plexus.util.FileUtils.filename;

/**
 * Use as a base class for testing {@link I18nScanner} implementations.
 */
abstract class AbstractI18nScannerTestCase {
    @Rule
    public final TemporaryFolder tempDir = TemporaryFolder.builder().assureDeletion().build();

    protected Collection<File> runI18nUsageFinder() {
        return new RecordI18nUsages().recordUsageForFilesInDirectory(tempDir.getRoot().getAbsolutePath());
    }

    protected void newFolder(final String... paths) throws IOException {
        tempDir.newFolder(paths);
    }

    /**
     * Given a test resource name, copy its contents to the test's temporary directory.
     * The new file will be given the same filename as the test resource and placed in the root of the test directory.
     * @param testResourceName either the filename of, or relative path to, a file in the `src/test/resources` directory
     *                         of this project.
     * @throws IOException if the file cannot be found or read to string
     */
    protected void copyTestResource(final String testResourceName) throws IOException {
        copyTestResource(testResourceName, filename(testResourceName));
    }

    /**
     * Given a test resource name, copy its contents to the test's temporary directory.
     * The new file will be placed at {@param tempFilePath} relative to the root of the test directory.
     * @param testResourceName either the filename of, or relative path to, a file in the `src/test/resources` directory
     *                         of this project.
     * @param tempFilePath a relative filepath to write the content to
     * @throws IOException if the file cannot be found or read to string, or if the content cannot be written to
     *                     the specified filepath (e.g., if some directories do not exist along the way)
     */
    protected void copyTestResource(final String testResourceName, final String tempFilePath) throws IOException {
        writeTestResource(getContentForTestResource(testResourceName), tempFilePath);
    }

    /**
     * Write string contents to a file in the test's temporary directory.
     * The new file will be placed at {@param tempFilePath} relative to the root of the test directory.
     * @param content what to write to the test resource
     * @param tempFilePath a relative filepath to write the content to
     * @throws IOException if the content cannot be written to
     *                     the specified filepath (e.g., if some directories do not exist along the way)
     */
    protected void writeTestResource(final String content, final String tempFilePath) throws IOException {
        final File tempFile = tempDir.newFile(tempFilePath);
        FileUtils.writeStringToFile(tempFile, content, UTF_8);
    }

    /**
     * Maps a collection of file handles to a collection of filepaths relative to the test's temporary directory.
     * @param files a collection of file handles
     * @return a collection of filepaths relative to the test's temporary directory
     */
    protected List<String> getRelativePaths(final Collection<File> files) {
        return getRelativePaths(files, tempDir.getRoot().toPath());
    }

    /**
     * Maps a collection of file handles to a collection of filepaths relative to the specified directory.
     * @param files a collection of file handles
     * @param rootPath a directory to treat as the relative root for generated filepaths
     * @return a collection of filepaths relative to the provided root path
     */
    protected static List<String> getRelativePaths(final Collection<File> files, @Nonnull final Path rootPath) {
        return files.stream()
                .map(File::toPath)
                .map(rootPath::relativize)
                .map(Path::toString)
                .collect(toList());
    }

    /**
     * Maps a collection of file handles to a collection of filenames with no directory information.
     * @param files a collection of file handles
     * @return a collection of filenames
     */
    protected static Collection<String> getFilenames(final Collection<File> files) {
        return files.stream().map(File::getName).collect(toList());
    }

    /**
     * Given a collection of file handles and a filename, if the filename exists in the collection,
     * return a {@link Properties} object by loading contents found in that file.
     * @param haystack a collection of file handles
     * @param filenameNeedle the name of a file to find in the haystack
     * @return a {@link Properties} object constructed by loading the contents of the found file
     * @throws IOException if the file cannot be found in the haystack,
     *                     or if the found file is not formatted like a properties file
     */
    protected static Properties getProperties(final Collection<File> haystack, final String filenameNeedle) throws IOException {
        final File file = haystack.stream()
                .filter(Objects::nonNull)
                .filter(f -> f.getAbsolutePath().endsWith(filenameNeedle))
                .findFirst()
                .orElseThrow(() -> new FileNotFoundException("File ending with '" + filenameNeedle + "' not found in list of files: " + haystack));
        Properties props = new Properties();
        props.load(new FileInputStream(file));
        return props;
    }

    private String getContentForTestResource(final String filepath) throws IOException {
        return getContent(getClass().getResource(filename(filepath)));
    }

    private static String getContent(@Nullable final URL url) throws IOException {
        return url != null ? getContent((BufferedInputStream)url.getContent()) : null;
    }

    private static String getContent(@Nonnull final InputStream is) {
        return new BufferedReader(new InputStreamReader(is, UTF_8))
                .lines()
                .collect(joining("\n"));
    }
}
