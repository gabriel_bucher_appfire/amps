package com.atlassian.maven.plugins.amps.rules;

import org.junit.rules.ExternalResource;
import org.junit.rules.TestRule;

import static java.util.Objects.requireNonNull;

public class PreserveSystemPropertyRule extends ExternalResource {

    /**
     * Factory method for a JUnit rule that preserves the value of the given system property.
     *
     * @param propertyName the property to preserve
     * @return a new rule instance
     */
    public static TestRule preserve(final String propertyName) {
        return new PreserveSystemPropertyRule(propertyName);
    }

    private final String propertyName;

    private String previousPropertyValue;

    private PreserveSystemPropertyRule(final String propertyName) {
        this.propertyName = requireNonNull(propertyName);
    }

    @Override
    protected void before() throws Throwable {
        previousPropertyValue = System.getProperty(propertyName);
    }

    @Override
    protected void after() {
        if (previousPropertyValue == null) {
            System.clearProperty(propertyName);
        } else {
            System.setProperty(propertyName, previousPropertyValue);
        }
    }
}
