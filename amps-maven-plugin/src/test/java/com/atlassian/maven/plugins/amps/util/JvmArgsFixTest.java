package com.atlassian.maven.plugins.amps.util;

import com.atlassian.maven.plugins.amps.product.JavaModulePackage;
import org.junit.Test;

import static org.apache.commons.lang3.JavaVersion.JAVA_1_8;
import static org.apache.commons.lang3.JavaVersion.JAVA_9;
import static org.apache.commons.lang3.SystemUtils.isJavaVersionAtLeast;
import static org.apache.commons.lang3.SystemUtils.isJavaVersionAtMost;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertEquals;
import static org.junit.Assume.assumeTrue;

public class JvmArgsFixTest {

    private final JvmArgsFix testDefaults = JvmArgsFix.empty().with("-Xmx", "512m");

    @Test
    public void testWithNullArgs() throws Exception {
        assertEquals("-Xmx512m", testDefaults.apply(null));
    }

    @Test
    public void testWithEmptyArgs() throws Exception {
        assertEquals("-Xmx512m", testDefaults.apply(""));
    }

    @Test
    public void testWithExistingUnrelated() throws Exception {
        assertEquals("-XmsSOMETHING -Xmx512m", testDefaults.apply("-XmsSOMETHING"));
    }

    @Test
    public void testWithMx() throws Exception {
        assertEquals("-XmxSOMETHING", testDefaults.apply("-XmxSOMETHING"));
    }

    @Test
    public void testWithBoth() throws Exception {
        assertEquals("-XmxSOMETHING -XX:MaxPermSize=SOMETHING", testDefaults.apply("-XmxSOMETHING -XX:MaxPermSize=SOMETHING"));
    }

    @Test
    public void testDefaults() throws Exception {
        // testing if what is put in defaults() is what is actually meant - plain text here
        assertEquals("-Xmx512m", JvmArgsFix.defaults().apply(null));
    }

    @Test
    public void testDefaultsOverride() throws Exception {
        assertEquals("-XmxSOMETHING -XX:MaxPermSize=STH", JvmArgsFix.defaults().with("-XX:MaxPermSize=", "STH").apply("-XmxSOMETHING"));
    }

    @Test
    public void testModuleArgsNotAddedPriorToJava8() {
        assumeTrue(isJavaVersionAtMost(JAVA_1_8));

        final String result = testDefaults
                .withAddOpens(new JavaModulePackage("foo", "bar"))
                .withAddExports(new JavaModulePackage("foo", "bar"))
                .apply(null);

        assertThat(result, allOf(
                not(containsString("add-opens")),
                not(containsString("add-exports"))));
    }

    @Test
    public void testModuleArgsAddedOnJava9OrHigher() {
        assumeTrue(isJavaVersionAtLeast(JAVA_9));

        final String result = testDefaults
                .withAddOpens(new JavaModulePackage("mod1", "package1"),
                        new JavaModulePackage("mod2", "package2"))
                .withAddExports(new JavaModulePackage("mod3", "package3"))
                .apply(null);

        assertThat(result, containsString(
                "--add-opens mod1/package1=ALL-UNNAMED " +
                        "--add-opens mod2/package2=ALL-UNNAMED " +
                        "--add-exports mod3/package3=ALL-UNNAMED"));
    }
}
