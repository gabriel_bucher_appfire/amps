package com.atlassian.maven.plugins.amps.database;

import com.atlassian.maven.plugins.amps.DataSource;
import com.atlassian.maven.plugins.amps.product.ImportMethod;
import org.apache.maven.plugin.logging.Log;
import org.codehaus.plexus.util.xml.Xpp3Dom;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;

import static java.nio.file.Files.createTempFile;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

public class Oracle10GTest extends DatabaseTestCase<Oracle10g> {

    private static final String PASSWORD = "thePassword";
    private static final String USERNAME = "theUsername";

    @Rule
    public final MockitoRule mockitoRule = MockitoJUnit.rule();

    @Mock
    private DataSource dataSource;

    @Mock
    private Log log;

    @InjectMocks
    private Oracle10g database;

    private void setUpImportMethod(final String importMethod) {
        when(dataSource.getImportMethod()).thenReturn(importMethod);
    }

    @Test
    public void testDatabaseName() {
        // Arrange
        final String schemaName = "theSchema";
        when(dataSource.getSchema()).thenReturn(schemaName);

        // Act
        final String name = database.getDatabaseName(dataSource);

        // Assert
        assertThat(name, is(schemaName));
    }

    @Test
    public void testCreateDatabaseSql() {
        // expected result
        final String dataPumpDir = File.separatorChar + "usr" + File.separatorChar + "home";
        final String expectedSqlFormat = "DECLARE\n"
                + "    v_count INTEGER := 0;\n"
                + "BEGIN\n"
                + "    SELECT COUNT (1) INTO v_count FROM dba_users WHERE username = UPPER ('product_user'); \n"
                + "    IF v_count != 0\n"
                + "    THEN\n"
                + "        EXECUTE IMMEDIATE('DROP USER product_user CASCADE');\n"
                + "    END IF;\n"
                + "    v_count := 0; \n"
                + "    SELECT COUNT (1) INTO v_count FROM dba_tablespaces WHERE tablespace_name = UPPER('product'); \n"
                + "    IF v_count != 0\n"
                + "    THEN\n"
                + "        EXECUTE IMMEDIATE('DROP TABLESPACE product INCLUDING CONTENTS AND DATAFILES');\n"
                + "    END IF;\n"
                + "    EXECUTE IMMEDIATE(q'{CREATE TABLESPACE product DATAFILE '/tmp/product.dbf' SIZE 32m AUTOEXTEND ON NEXT 32m MAXSIZE 4096m EXTENT MANAGEMENT LOCAL}');\n"
                + "    EXECUTE IMMEDIATE('CREATE USER product_user IDENTIFIED BY product_pwd DEFAULT TABLESPACE product QUOTA UNLIMITED ON product');\n"
                + "    EXECUTE IMMEDIATE('GRANT CONNECT, RESOURCE, IMP_FULL_DATABASE TO product_user');\n"
                + "    EXECUTE IMMEDIATE(q'{CREATE OR REPLACE DIRECTORY DATA_PUMP_DIR AS '%s'}');\n"
                + "    EXECUTE IMMEDIATE('GRANT READ, WRITE ON DIRECTORY DATA_PUMP_DIR TO product_user');\n"
                + "END;\n"
                + "/";
        final String expectedSql = String.format(expectedSqlFormat, dataPumpDir);

        // setup
        when(dataSource.getDriver()).thenReturn("oracle.jdbc.OracleDriver");
        when(dataSource.getDumpFilePath()).thenReturn("/usr/home/oracle.bak");
        when(dataSource.getPassword()).thenReturn("product_pwd");
        when(dataSource.getSystemUrl()).thenReturn("theSystemUrl");
        when(dataSource.getSystemUsername()).thenReturn("theSystemUser");
        when(dataSource.getUsername()).thenReturn("product_user");

        // execute
        final Xpp3Dom sqlMavenCreateConfiguration = database.getSqlMavenCreateConfiguration(dataSource);

        // assert
        final String initDatabaseSQL = sqlMavenCreateConfiguration.getChild("sqlCommand").getValue();
        assertThat(initDatabaseSQL, is(expectedSql));
    }

    @Test
    public void testImportConfiguration() throws Exception {
        // Arrange
        setUpImportMethod(ImportMethod.IMPDP.name());
        setUpDataSourceCredentials();
        final Path dumpsDirectory = Paths.get(System.getProperty("java.io.tmpdir"), getClass().getSimpleName());
        dumpsDirectory.toFile().deleteOnExit();
        dumpsDirectory.toFile().mkdirs();
        final Path dumpFilePath = createTempFile(dumpsDirectory, "my_dump", ".bin");
        when(dataSource.getDumpFilePath()).thenReturn(dumpFilePath.toString());

        // Act
        final Xpp3Dom importConfiguration = database.getExecMavenToolImportConfiguration(dataSource);

        // Assert
        assertNotNull(importConfiguration);
        assertThat(importConfiguration.getChild("executable").getValue(), is("impdp"));
        final Xpp3Dom arguments = importConfiguration.getChild("arguments");
        assertThat(arguments.getChildCount(), is(3));
        assertThat(arguments.getChild(0).getValue(), is("theUsername/thePassword"));
        assertThat(arguments.getChild(1).getValue(), is("DUMPFILE=" + dumpFilePath.getFileName().toString()));
        assertThat(arguments.getChild(2).getValue(), is("DIRECTORY=DATA_PUMP_DIR"));
        assertCanReadWriteAndExecute(dumpFilePath.toFile());
        assertCanReadWriteAndExecute(dumpFilePath.getParent().toFile());
    }

    private void assertCanReadWriteAndExecute(final File file) {
        assertTrue(file.canExecute());
        assertTrue(file.canRead());
        assertTrue(file.canWrite());
    }

    private void setUpDataSourceCredentials() {
        when(dataSource.getUsername()).thenReturn(USERNAME);
        when(dataSource.getPassword()).thenReturn(PASSWORD);
    }

    @Test
    public void execMavenImportConfigurationWhenNotUsingImpDpToolShouldBeNull() throws Exception {
        // Arrange
        setUpImportMethod("NOT_impdp");

        // Act
        final Xpp3Dom importConfiguration = database.getExecMavenToolImportConfiguration(dataSource);

        // Assert
        assertNull(importConfiguration);
    }

    @Override
    protected Oracle10g getDatabase() {
        return database;
    }
}
