package com.atlassian.maven.plugins.amps;

import org.apache.maven.project.MavenProject;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import java.util.Locale;

import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class RunMojoTest {

    @Rule
    public final MethodRule mockitoRule = MockitoJUnit.rule();

    @InjectMocks
    private RunMojo runMojo;

    @Mock
    private MavenContext mavenContext;

    @Mock
    private MavenProject currentProject;

    @Before
    public void setUp() {
        runMojo.mavenContext = mavenContext;
        when(mavenContext.getProject()).thenReturn(currentProject);
    }

    @Test
    public void shouldSkipCurrentProject_whenRunProjectIsNotCurrentProject_shouldReturnTrue() {
        // Arrange
        runMojo.runProject = "theRunProject";
        when(currentProject.getArtifactId()).thenReturn("someOtherProject");

        // Act
        final boolean skip = runMojo.shouldSkipCurrentProject();

        // Assert
        assertThat(skip, is(true));
    }

    @Test
    public void shouldSkipCurrentProject_whenRunProjectIsCurrentProject_shouldReturnFalse() {
        // Arrange
        runMojo.runProject = "theRunProject";
        when(currentProject.getArtifactId()).thenReturn(runMojo.runProject.toLowerCase(Locale.ROOT));

        // Act
        final boolean skip = runMojo.shouldSkipCurrentProject();

        // Assert
        assertThat(skip, is(false));
    }

    @Test
    public void shouldSkipCurrentProject_whenRunProjectBlankAndLastProjectFalse_shouldReturnFalse() {
        // Arrange
        runMojo.runProject = "";
        runMojo.runLastProject = false;

        // Act
        final boolean skip = runMojo.shouldSkipCurrentProject();

        // Assert
        assertThat(skip, is(false));
    }

    @Test
    public void shouldSkipCurrentProject_whenRunProjectBlankAndLastProjectTrueAndNullReactor_shouldReturnFalse() {
        // Arrange
        runMojo.runProject = "";
        runMojo.runLastProject = true;
        when(mavenContext.getReactor()).thenReturn(null);

        // Act
        final boolean skip = runMojo.shouldSkipCurrentProject();

        // Assert
        assertThat(skip, is(false));
    }

    @Test
    public void shouldSkipCurrentProject_whenRunProjectBlankAndLastProjectTrueAndEmptyReactor_shouldReturnFalse() {
        // Arrange
        runMojo.runProject = "";
        runMojo.runLastProject = true;
        when(mavenContext.getReactor()).thenReturn(emptyList());

        // Act
        final boolean skip = runMojo.shouldSkipCurrentProject();

        // Assert
        assertThat(skip, is(false));
    }

    @Test
    public void shouldSkipCurrentProject_whenRunProjectBlankLastProjectTrueAndNotLastInReactor_shouldReturnTrue() {
        // Arrange
        runMojo.runProject = "";
        runMojo.runLastProject = true;
        final MavenProject lastProject = mock(MavenProject.class);
        when(mavenContext.getReactor()).thenReturn(asList(currentProject, lastProject));

        // Act
        final boolean skip = runMojo.shouldSkipCurrentProject();

        // Assert
        assertThat(skip, is(true));
    }

    @Test
    public void shouldSkipCurrentProject_whenRunProjectBlankLastProjectTrueAndLastInReactor_shouldReturnFalse() {
        // Arrange
        runMojo.runProject = "";
        runMojo.runLastProject = true;
        final MavenProject firstProject = mock(MavenProject.class);
        when(mavenContext.getReactor()).thenReturn(asList(firstProject, currentProject));

        // Act
        final boolean skip = runMojo.shouldSkipCurrentProject();

        // Assert
        assertThat(skip, is(false));
    }
}
