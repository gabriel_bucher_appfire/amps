package com.atlassian.maven.plugins.amps.util;

import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class VersionUtilsTest {

    @Test
    public void getVersion_whenPomPropertiesNotFound_shouldLiterallyReturnRelease() {
        // Act
        final String version = VersionUtils.getVersion();

        // Assert
        assertThat(version, is("RELEASE"));
    }
}
