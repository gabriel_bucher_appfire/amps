package com.atlassian.maven.plugins.amps;

import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertNotNull;

public class NodeTest {

    @Test
    public void getDebugArgs_whenDebugArgsNotSet_shouldNotReturnNull() {
        // Arrange
        final Node node = new Node();

        // Act
        final String debugArgs = node.getDebugArgs();

        // Assert
        assertNotNull(debugArgs);
    }

    @Test
    public void defaultJvmDebugArgs_whenSuspendFieldIsTrue_shouldSuspend() {
        // Arrange
        final Node node = new Node();
        node.setJvmDebugPort(42);
        node.setJvmDebugSuspend(true);

        // Act
        node.defaultDebugArgs(false);

        // Assert
        assertThat(node.getDebugArgs(), containsString("suspend=y"));
    }

    @Test
    public void defaultJvmDebugArgs_whenSuspendFieldIsFalse_shouldNotSuspend() {
        // Arrange
        final Node node = new Node();
        node.setJvmDebugPort(42);
        node.setJvmDebugSuspend(false);

        // Act
        node.defaultDebugArgs(true);

        // Assert
        assertThat(node.getDebugArgs(), containsString("suspend=n"));
    }

    @Test
    public void defaultJvmDebugArgs_whenSuspendFieldIsNullAndDefaultIsTrue_shouldSuspend() {
        // Arrange
        final Node node = new Node();
        node.setJvmDebugPort(42);

        // Act
        node.defaultDebugArgs(true);

        // Assert
        assertThat(node.getDebugArgs(), containsString("suspend=y"));
    }

    @Test
    public void defaultJvmDebugArgs_whenSuspendFieldIsNullAndDefaultIsFalse_shouldNotSuspend() {
        // Arrange
        final Node node = new Node();
        node.setJvmDebugPort(42);

        // Act
        node.defaultDebugArgs(false);

        // Assert
        assertThat(node.getDebugArgs(), containsString("suspend=n"));
    }

    @Test
    public void ensurePortsSet_whenAllPortsAreZero_shouldSetThemAll() {
        // Arrange
        final Node node = new Node();
        final String instanceId = "anything";

        // Act
        node.ensureNonDebugPortsAreSet(instanceId);
        node.ensureNonDebugPortsAreSet(instanceId); // check for idempotency

        // Assert
        assertThat(node.getAjpPort(), not(0));
        assertThat(node.getRmiPort(), not(0));
        assertThat(node.getWebPort(), not(0));
        assertThat(node.getJvmDebugPort(), is(0));
    }
}
