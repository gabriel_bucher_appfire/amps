package com.atlassian.maven.plugins.amps.i18n;

import org.hamcrest.Matcher;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.util.Collection;
import java.util.Properties;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.aMapWithSize;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.hasEntry;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;

public class RecordI18nUsagesTest extends AbstractI18nScannerTestCase {
    private static final String PREWRITTEN_CONTENT = "# Some comment\nalready.written=100";
    private static final Matcher<? super Properties> EXPECTED_VALUES = allOf(hasEntry("already.written", "100"), aMapWithSize(1));

    @Rule
    public final TemporaryFolder realFolder = TemporaryFolder.builder().assureDeletion().build();

    @Test
    public void testSkipsFileWhenOutputFileAlreadyExists() throws Exception {
        writeTestResource(PREWRITTEN_CONTENT, "single.js.i18n.properties");
        copyTestResource("single.js");
        Collection<File> results = runI18nUsageFinder();
        assertThat(getProperties(results, "single.js.i18n.properties"), EXPECTED_VALUES);
    }

    @Test
    public void testSkipsMinifiedFilesWhenOutputFileAlreadyExistsForBaseFilename() throws Exception {
        writeTestResource(PREWRITTEN_CONTENT, "single.js.i18n.properties");
        copyTestResource("single.js", "single.min.js");
        copyTestResource("single.js", "single-min.js");
        Collection<File> results = runI18nUsageFinder();
        assertThat(results, hasSize(1));
        assertThat(getProperties(results, "single.js.i18n.properties"), EXPECTED_VALUES);
    }

    @Test
    public void testSkipsScanWhenPathIsEmptyString() throws Exception {
        Collection<File> results = new RecordI18nUsages().recordUsageForFilesInDirectory("");
        assertThat(results, is(empty()));
    }

    @Test
    public void testSkipsScanWhenPathDoesNotExist() throws Exception {
        final File realDirectory = realFolder.getRoot();
        final File noDirectory = new File(realDirectory, "nonexistent");
        Collection<File> results = new RecordI18nUsages().recordUsageForFilesInDirectory(noDirectory);
        assertThat(results, is(empty()));
    }

    @Test
    public void testReturnsNothingWhenPathIsEmptyFolder() throws Exception {
        final File realDirectory = realFolder.getRoot();
        Collection<File> results = new RecordI18nUsages().recordUsageForFilesInDirectory(realDirectory);
        assertThat(results, is(empty()));
    }
}
