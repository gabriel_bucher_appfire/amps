package com.atlassian.maven.plugins.amps.product;

import org.junit.Test;

import static com.atlassian.maven.plugins.amps.product.ImportMethod.IMPDP;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class ImportMethodTest {

    @Test
    public void getValueOf_shouldBeCaseInsensitive() {
        assertIMPDP("IMPDP");
        assertIMPDP("impdp");
        assertIMPDP("iMpdp");
    }

    private void assertIMPDP(final String input) {
        assertThat("Import method should be IMPDP", ImportMethod.getValueOf(input), equalTo(IMPDP));
    }
}
