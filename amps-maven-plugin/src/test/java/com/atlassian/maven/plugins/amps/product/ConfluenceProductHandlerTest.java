package com.atlassian.maven.plugins.amps.product;

import com.atlassian.maven.plugins.amps.MavenContext;
import com.atlassian.maven.plugins.amps.MavenGoals;
import com.atlassian.maven.plugins.amps.Node;
import com.atlassian.maven.plugins.amps.Product;
import com.atlassian.maven.plugins.amps.ProductArtifact;
import com.atlassian.maven.plugins.amps.XmlOverride;
import com.atlassian.maven.plugins.amps.product.manager.WebAppManager;
import com.atlassian.maven.plugins.amps.util.ArtifactRetriever;
import com.atlassian.maven.plugins.amps.util.ConfigFileUtils.Replacement;
import org.apache.maven.artifact.resolver.ArtifactResolver;
import org.apache.maven.model.Build;
import org.apache.maven.plugin.logging.Log;
import org.apache.maven.project.MavenProject;
import org.apache.maven.repository.RepositorySystem;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import java.io.File;
import java.net.URL;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import static com.atlassian.maven.plugins.amps.product.ConfluenceProductHandler.HAZELCAST_LISTEN_PORT;
import static com.atlassian.maven.plugins.amps.product.ConfluenceProductHandler.SYNCHRONY_PORT;
import static com.atlassian.maven.plugins.amps.product.ConfluenceProductHandler.SYNCHRONY_PROXY_VERSION;
import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static java.util.Collections.singletonMap;
import static java.util.Objects.requireNonNull;
import static org.apache.commons.lang3.JavaVersion.JAVA_9;
import static org.apache.commons.lang3.SystemUtils.isJavaVersionAtLeast;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.not;
import static org.junit.Assume.assumeTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class ConfluenceProductHandlerTest {

    private static final String BUILD_DIRECTORY = "theBuildDirectory";

    private static final String LATEST_SYNCHRONY_PROXY_VERSION = "2.0.1";

    @Rule
    public final MethodRule mockitoRule = MockitoJUnit.rule();

    // Can't use @InjectMocks because of method calls on constructor args
    private ConfluenceProductHandler confluenceProductHandler;

    @Mock
    private RepositorySystem repositorySystem;

    @Mock
    private ArtifactResolver artifactResolver;

    @Mock
    private ArtifactRetriever artifactRetriever;

    @Mock
    private Build build;

    @Mock
    private Log log;

    @Mock
    private MavenContext context;

    @Mock
    private MavenGoals goals;

    @Mock
    private MavenProject project;

    @Mock
    private Product product;

    @Mock
    private WebAppManager webAppManager;

    @Captor
    private ArgumentCaptor<Collection<XmlOverride>> expectedOverridesCaptor;

    private String oldSynchronyProxyEnv;

    private File homeDir;

    @Before
    public void setUp() throws Exception {
        oldSynchronyProxyEnv = System.getProperty(SYNCHRONY_PROXY_VERSION);
        when(build.getDirectory()).thenReturn(BUILD_DIRECTORY);
        when(project.getBuild()).thenReturn(build);
        when(context.getLog()).thenReturn(log);
        when(context.getProject()).thenReturn(project);
        confluenceProductHandler =
                new ConfluenceProductHandler(context, goals, repositorySystem, artifactResolver, webAppManager);
        final URL configFileUrl = requireNonNull(getClass().getResource("confluence.cfg.xml"));
        homeDir = new File(configFileUrl.toURI()).getParentFile();
    }

    @After
    public void tearDown() {
        if (oldSynchronyProxyEnv != null) {
            System.setProperty(SYNCHRONY_PROXY_VERSION, oldSynchronyProxyEnv);
        } else {
            System.clearProperty(SYNCHRONY_PROXY_VERSION);
        }
    }

    @Test
    public void testCustomiseInstanceBefore6() throws Exception {
        when(product.getVersion()).thenReturn("5.10.8");
        confluenceProductHandler.customiseInstance(product, new File("./"), new File("./"));
        verify(goals, never()).copyWebappWar(any(), any(), any());
    }

    @Test
    public void testCustomiseInstanceBefore6_5() throws Exception {
        ConfluenceProductHandler spied = spy(confluenceProductHandler);
        when(product.getSharedHome()).thenReturn("theSharedHome");
        when(product.getVersion()).thenReturn("6.4.0-SNAPSHOT");
        when(goals.copyWebappWar(any(), any(), anyString())).thenReturn(new File("./"));
        doReturn(new File("./")).when(spied).getBaseDirectory(product);

        spied.customiseInstance(product, homeDir, new File("./"));

        ArgumentCaptor<ProductArtifact> synchronyProxyArtifactCaptor = ArgumentCaptor.forClass(ProductArtifact.class);
        verify(goals).copyWebappWar(synchronyProxyArtifactCaptor.capture(), any(File.class), anyString());
        assertThat(synchronyProxyArtifactCaptor.getValue().getVersion(), is("1.0.17"));
    }

    @Test
    public void testCustomiseInstanceOverridden() throws Exception {
        System.setProperty(SYNCHRONY_PROXY_VERSION, "1.0.16");
        when(product.getSharedHome()).thenReturn("theSharedHome");
        when(product.getVersion()).thenReturn("6.4.0-SNAPSHOT");
        when(goals.copyWebappWar(any(), any(), anyString())).thenReturn(new File("./"));
        ConfluenceProductHandler spied = spy(confluenceProductHandler);
        doReturn(new File("./")).when(spied).getBaseDirectory(product);

        spied.customiseInstance(product, homeDir, new File("./"));

        ArgumentCaptor<ProductArtifact> synchronyProxyArtifactCaptor = ArgumentCaptor.forClass(ProductArtifact.class);
        verify(goals).copyWebappWar(synchronyProxyArtifactCaptor.capture(), any(File.class), anyString());
        assertThat(synchronyProxyArtifactCaptor.getValue().getVersion(), is("1.0.16"));
    }

    @Test
    public void testCustomiseInstanceAfter6_5() throws Exception {
        when(product.getArtifactRetriever()).thenReturn(artifactRetriever);
        when(product.getSharedHome()).thenReturn("theSharedHome");
        when(product.getVersion()).thenReturn("6.5.0-SNAPSHOT");
        when(artifactRetriever.getLatestStableVersion(any())).thenReturn(LATEST_SYNCHRONY_PROXY_VERSION);
        when(goals.copyWebappWar(any(), any(), anyString())).thenReturn(new File("./"));
        ConfluenceProductHandler spied = spy(confluenceProductHandler);
        doReturn(new File("./")).when(spied).getBaseDirectory(product);

        spied.customiseInstance(product, homeDir, new File("./"));

        ArgumentCaptor<ProductArtifact> synchronyProxyArtifactCaptor = ArgumentCaptor.forClass(ProductArtifact.class);
        verify(goals).copyWebappWar(synchronyProxyArtifactCaptor.capture(), any(File.class), anyString());
        assertThat(synchronyProxyArtifactCaptor.getValue().getVersion(), is(LATEST_SYNCHRONY_PROXY_VERSION));
    }

    @Test
    public void testCustomiseInstanceForServerXmlOverrides() throws Exception {
        // Setup
        when(product.getArtifactRetriever()).thenReturn(artifactRetriever);
        when(product.getSharedHome()).thenReturn("theSharedHome");
        when(product.getVersion()).thenReturn("6.5.0-SNAPSHOT");
        when(artifactRetriever.getLatestStableVersion(any())).thenReturn(LATEST_SYNCHRONY_PROXY_VERSION);
        when(goals.copyWebappWar(any(), any(), anyString())).thenReturn(new File("./"));
        ConfluenceProductHandler spied = spy(confluenceProductHandler);
        doReturn(new File("./")).when(spied).getBaseDirectory(product);

        // Execute
        spied.customiseInstance(product, homeDir, new File("./"));

        // Verify
        verify(product).setCargoXmlOverrides(expectedOverridesCaptor.capture());
        assertThat(expectedOverridesCaptor.getValue().size(), is(1));
        final XmlOverride xmlOverride = expectedOverridesCaptor.getValue().stream().findFirst()
                .orElseThrow(() -> new AssertionError("No XML override"));
        assertThat(xmlOverride.getFile(), is("conf/server.xml"));
        assertThat(xmlOverride.getAttributeName(), is("maxThreads"));
        assertThat(xmlOverride.getxPathExpression(), is("//Connector"));
        assertThat(xmlOverride.getValue(), is("48"));
    }

    @Test
    public void getReplacements_shouldReplacePort8080PlaceholderWithCorrectBaseUrl() {
        // Set up
        final Product product = mock(Product.class);
        when(product.getContextPath()).thenReturn("theContextPath");
        when(product.getDataHome()).thenReturn("theProjectDir/theDataHome");
        when(product.getInstanceId()).thenReturn("theInstanceId");
        when(product.getProtocol()).thenReturn("theProtocol");
        when(product.getServer()).thenReturn("theServer");
        when(product.getWebPortForNode(0)).thenReturn(42);
        final Node node = mock(Node.class);
        when(product.getNodes()).thenReturn(singletonList(node));

        // Execute
        final List<Replacement> replacements = confluenceProductHandler.getReplacements(product, 0);

        // Verify
        final Replacement lastReplacement = replacements.get(replacements.size() - 1);
        final String replaced = lastReplacement.replace("<baseUrl>http://localhost:8080</baseUrl>");
        assertThat(replaced, is("<baseUrl>theProtocol://theServer:42/theContextPath</baseUrl>"));
    }

    @Test
    public void getProductSpecificSystemProperties_whenTwoNodes_shouldAllowLoopbackCluster() {
        // Arrange
        final Node node0 = mock(Node.class);
        final Node node1 = mock(Node.class);
        when(product.getInstanceId()).thenReturn("theInstanceId");
        when(product.getNodes()).thenReturn(asList(node0, node1));
        when(product.isMultiNode()).thenReturn(true);
        when(project.getBuild()).thenReturn(build);
        when(build.getDirectory()).thenReturn("theBuildDirectory");
        when(node0.getSystemProperties()).thenReturn(singletonMap(SYNCHRONY_PORT, "42"));

        // Act
        final Map<String, String> node0Properties =
                confluenceProductHandler.getProductSpecificSystemProperties(product, 0);
        final Map<String, String> node1Properties =
                confluenceProductHandler.getProductSpecificSystemProperties(product, 1);

        // Assert
        final String allowLoopbackClusterProperty = "confluence.allow.loopback.cluster";
        assertThat(node0Properties.get(allowLoopbackClusterProperty), is(Boolean.toString(true)));
        assertThat(node1Properties.get(allowLoopbackClusterProperty), is(Boolean.toString(true)));

        final String node0HazelcastPort = node0Properties.get(HAZELCAST_LISTEN_PORT);
        final String node1HazelcastPort = node1Properties.get(HAZELCAST_LISTEN_PORT);
        assertThat(node0HazelcastPort, is(not("0")));
        assertThat(node1HazelcastPort, is(not("0")));
        assertThat(node1HazelcastPort, is(node0HazelcastPort));

        final String nodeNameProperty = "confluence.cluster.node.name";
        assertThat(node0Properties.get(nodeNameProperty), is("theInstanceId-0"));
        assertThat(node1Properties.get(nodeNameProperty), is("theInstanceId-1"));
    }

    @Test
    public void testFixJvmArgs() {
        assumeTrue(isJavaVersionAtLeast(JAVA_9));

        confluenceProductHandler.fixJvmArgs(product);

        final ArgumentCaptor<String> captor = ArgumentCaptor.forClass(String.class);

        verify(product).setJvmArgs(captor.capture());

        assertThat(captor.getValue(), allOf(
                containsString("-Xmx4g -Xms1g"),
                containsString("--add-opens")));
    }
}
