package com.atlassian.maven.plugins.amps.analytics.impl;

import com.atlassian.maven.plugins.amps.analytics.event.AnalyticsEvent;
import com.atlassian.maven.plugins.amps.analytics.visitordata.VisitorDataDao;
import com.dmurph.tracking.AnalyticsConfigData;
import com.dmurph.tracking.JGoogleAnalyticsTracker;
import com.dmurph.tracking.VisitorData;
import org.apache.maven.plugin.logging.Log;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.junit.rules.TestRule;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import static com.atlassian.maven.plugins.amps.rules.PreserveSystemPropertyRule.preserve;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class GoogleAnalyticsServiceTest {

    @Rule
    public final MethodRule mockitoRule = MockitoJUnit.rule();

    @Rule
    public final TestRule preserveHttpAgentRule = preserve(HTTP_AGENT_PROPERTY);

    private static final String EVENT_ACTION = "theAction";
    private static final String EVENT_CATEGORY = "theEventCategory";
    private static final String EVENT_LABEL = "theLabel";
    private static final String HTTP_AGENT_PROPERTY = "http.agent";
    private static final String USER_AGENT = "theUserAgent";

    private GoogleAnalyticsService googleAnalyticsService;

    @Mock
    private AnalyticsConfigData config;

    @Mock
    private JGoogleAnalyticsTracker tracker;

    @Mock
    private Log log;

    @Mock
    private UserAgent userAgent;

    @Mock
    private VisitorDataDao visitorDataDao;

    @Before
    public void setUp() {
        googleAnalyticsService = new GoogleAnalyticsService(
                config, tracker, log, EVENT_CATEGORY, userAgent, visitorDataDao);
    }

    @Test
    public void send_whenCalled_shouldCorrectlyInvokeCollaborators() {
        // Arrange
        final AnalyticsEvent event = mock(AnalyticsEvent.class);
        when(event.getAction()).thenReturn(EVENT_ACTION);
        when(event.getLabel()).thenReturn(EVENT_LABEL);
        when(event.toString()).thenReturn("actionPlusLabel");
        final VisitorData visitorData = mock(VisitorData.class);
        when(config.getVisitorData()).thenReturn(visitorData);
        when(userAgent.getHeaderValue()).thenReturn(USER_AGENT);

        // Act
        googleAnalyticsService.send(event);

        // Assert
        assertThat(System.getProperty(HTTP_AGENT_PROPERTY), is(USER_AGENT));
        verify(log).info("Sending event to Google Analytics: theEventCategory - actionPlusLabel");
        verify(tracker).trackEvent(EVENT_CATEGORY, EVENT_ACTION, EVENT_LABEL);
        verify(visitorDataDao).save(visitorData);
    }
}
