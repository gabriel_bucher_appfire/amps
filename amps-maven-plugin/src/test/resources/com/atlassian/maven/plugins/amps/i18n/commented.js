/**
 * @fileOverview
 * code comments... fun times!
 * check whether mentions of WRM.I18n.getText('plugin.i18n.commented-in-file-overview-jsdoc')
 * in comments is found or not.
 */

// technically, commented-out code could still be considered usage of I18n...
// WRM.I18n.getText('plugin.i18n.commented-one-line')

/*
 WRM.I18n.getText("plugin.i18n.commented-multi-line")
*/
