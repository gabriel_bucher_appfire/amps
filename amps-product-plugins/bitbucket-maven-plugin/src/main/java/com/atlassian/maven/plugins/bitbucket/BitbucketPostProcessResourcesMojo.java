package com.atlassian.maven.plugins.bitbucket;

import com.atlassian.maven.plugins.amps.PostProcessResourcesMojo;
import org.apache.maven.plugins.annotations.Mojo;

@Mojo(name="post-process-resources")
public class BitbucketPostProcessResourcesMojo extends PostProcessResourcesMojo {
}
