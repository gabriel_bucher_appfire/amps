package com.atlassian.maven.plugins.confluence;

import com.atlassian.maven.plugins.amps.PostProcessResourcesMojo;
import org.apache.maven.plugins.annotations.Mojo;

@Mojo(name="post-process-resources")
public class ConfluencePostProcessResourcesMojo extends PostProcessResourcesMojo {
}
