/**
 * Provides Jira-specific static factories for retrieving the Jira-specific list of choices found by ASM.
 *
 * @since 3.6
 */
package com.atlassian.maven.plugins.amps.codegen.jira;